var min_scroll = 600;
var listing_period_type = 'all';
var listing_type = 'activity';

var activity_listing_start = 0;
var activity_listing_limit=40;
var activity_last_scroll_no = 0;

var deal_listing_start = 0;
var deal_listing_limit=40;
var deal_last_scroll_no = 0;
var DEAL_IMAGES_URL_PREFIX = "http://magicpin.in/media/catalog/product/";



$(document).ready(function() {
	fill_activities(activity_listing_limit);
	fill_deals(deal_listing_limit);
	activate_page_links();
	$("[id$='_tab']").click(function() {
		var id_parts = this.id.split("_");
		$("class$=['_listing']").hide();
		$("."+id_parts[0]+"_listing").show();
		listing_type = id_parts[0];
	});
});

function enable_onscroll() {
	$(window).scroll(function (event) {
    	var scroll = $(window).scrollTop();
    	var scroll_no =parseInt(scroll/min_scroll);
    	if(listing_type == 'activity') {
    		if(scroll_no>activity_last_scroll_no) {
    			activity_last_scroll_no = scroll_no;
    			fill_activities(((scroll_no+1) * activity_listing_limit) - activity_listing_start);
    		}
    	}
    	else if(listing_type == 'deal') {
    		if(scroll_no>deal_last_scroll_no) {
    			deal_last_scroll_no = scroll_no;
    			fill_deals(((scroll_no+1) * deal_listing_limit) - deal_listing_start);
    		}
    	}
	});
}
function fill_activities(scroll_limit) {
	$.ajax({method:'post',url:GET_TRANSACTIONS_CUSTOMER_URL+listing_period_type+'/'+activity_listing_start+"/"+scroll_limit,success:function(response) {
		response = JSON.parse(response);
		var str = "";
		var today = new Date();
		today = [today.getFullYear(),today.getMonth()+1,
               today.getDate()].join('-')+' '+
              [today.getHours(),
               today.getMinutes(),
               today.getSeconds()].join(':');
        today = moment(today,"YYYY-MM-DD HH:mm:ss");
		$.each(response,function(index,value) {
			var txn_date_arr = value.txn_time.split("T");
			var txn_time_arr = txn_date_arr[1].split("Z");
			value.txn_time = txn_date_arr[0]+" "+txn_time_arr[0];
			var time_str = moment.duration(today.diff(value.txn_time,"YYYY-MM-DD HH:mm:ss")).humanize() +' ago';
			if(time_str == 'a day ago') {
				time_str = 'Yesterday';
			}
			var sku_parts = value.sku.trim().split("#");
			var selfie = value.selfie_url.trim();
			var selfie_parts = selfie.split("/");
			if(selfie_parts[0] !='http' && selfie_parts[0]!='https') 
				selfie = "http://"+selfie;
			var amount = value.recharge_amt;
			str += '<div class="col-md-4 col-sm-8 col-xs-12">';
            str += '<div class="manage-deal-wrapper ">';
			str += '<div class="customer-profile-wrapper">';
			str += '<div class="customer-profile-details">';
			str += '<div class="price-box">';
			//str += '<h4 class="store-view-hd">'+sku_parts[0]+'</h4>';
			str += '</div>';
			str += '</div>';
			str += '<a href="'+selfie+'" class="fancybox"><img class="img-responsive" src="'+selfie+'"></a>';
			str += '</div>';
			str += '</div>';
			str += '<div class="store-view-deatail-block">';
			str += '<div class="time-info"><span class="icon"><i class=" fa fa-clock-o"></i></span> '+time_str+' </div>';
			//str += '<p>गाय  का  प्रस्ताव </p>';
			str += '</div>';
			str += '<div class="share-wraper ">';
			str += '<div class="row">';
			str += '<div class="col-md-6 col-sm-6 col-xs-12 share-link">';
			str += '<div class="share-block">';
			//str += '<a>25 Likes</a>';
			str += '</div>';
			str += '</div>';
			str += '<div class="col-md-6 col-sm-6 col-xs-12 share-link">';
			str += '<div class="share-block">';
			//str += '<a>25 Comments</a>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
		});
		activity_listing_start = activity_listing_start + scroll_limit;
		$("#actiity_listing_loader").hide();
		$(".activity_listing").append(str);
	}
	});
}
function fill_deals(scroll_limit) {

	$.ajax({method:'post',url:GET_DEALS_CUSTOMER_URL+listing_period_type+'/'+deal_listing_start+"/"+scroll_limit,success:function(response) {
		var str = "";
		response = JSON.parse(response);
		$.each(response,function(index,value) {
			var sku_parts = value.SKU.trim().split("#");
			var selfie = value.img_url.trim();
			selfie = DEAL_IMAGES_URL_PREFIX+selfie;
			var amount = value.price;
			var locality = value.locality;
			var cashback = (parseFloat(value.cashback_percent)*parseFloat(amount))/100.00;

			str += '<li class="product-item-custom col-md-4 col-sm-6 col-xs-12">';
			str += '<h2><a title="'+sku_parts[0]+'" href="http://magicpin.in/deals/search/all/'+value.merchant_name+'">'+value.merchant_name+'</a></h2>';
			str += '<div class="product-detail-wrapper">';
			str += '<div class="product-info-wrapper">';
			str += '<h3>'+sku_parts[0]+'</h3>';
			str += '<div class="product-rating-stars">';
			str += '<div title="Rating:2/3" data-placement="top" data-toggle="tooltip" class="ratings">';
			//str += '<span><img alt="magicpin" src="https://magicpin.in/deals/images/star.png"></span>';
			//str += '<span><img alt="magicpin" src="http://magicpin.in/deals/images/star.png"></span>';
			//str += '<span><img alt="magicpin" src="http://magicpin.in/deals/images/star-blank.png"></span>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '<a class="product-image" title="'+sku_parts[0]+'" href="#">';
			str += '<img alt="'+sku_parts[0]+'" src="'+selfie+'">';
			str += '</a>';
			str += '</div>';
			str += '<div class="product-info-custom">';
			str += '<div class="price-box">';
			str += '<div class="special-price">';
			str += '<span class="label">Cash Back:</span><span class="price"><i class="fa fa-inr"></i>'+cashback+'</span>';
			str += '</div>';
			str += '<div class="special-price pull-right">';
			str += '<span class="distance">'+value.locality+'</span>';
			str += '</div>';
			str += '</div>';
			str += '<div class="price-box category-wrapper">';
			str += '<div class="special-price dealcategory-price">';
			str += '<span class="label">Cost for Two:</span><span class="price"><i class="fa fa-inr"></i>'+value.cost_for_two+'</span>';
			str += '</div>';
			str += '<div class="category pull-right">';
			str += '</div>';
			str += '<div class="category pull-right">';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</li>';
		});
		deal_listing_start = deal_listing_start + scroll_limit;
		$("#deal_listing_loader").hide();
		$(".deal_listing").append(str);
	}
	});
}