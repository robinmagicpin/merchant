var uploaded_images_index = 1;
var chosen_user_name = true;
$(document).ready(function() {
	fill_states();
	prefill_locations();
	search_user_name();
	fill_store_images();
	close_image();
	$("[name='store_image']").change(function(e) {
		
		if($(".edit_profile_image_upload_loader").length == 0) {
			$(".profile-thumb form .bootstrap-filestyle").prepend('<div class="edit_profile_image_upload_loader image_upload_button"><i class="fa fa-cog fa-spin"></i></div>');
		}
		$(".image_form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			$("#image_submit").hide();
			$(".edit_profile_image_upload_loader").show();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				$(".profile-thumb form .bootstrap-filestyle").append("<div class='store_imgs_div' id='store_images_div_"+uploaded_images_index+"'><img id='store_images_"+uploaded_images_index+"' src='"+response.image_url+"' /><a id='store_images_close_"+uploaded_images_index+"'><i class='fa fa-times-circle-o'></i></a><input type='hidden' id='store_images_dimension_div_"+uploaded_images_index+"' value='"+response.width+"_"+response.height+"' /></div>");
				uploaded_images_index = uploaded_images_index+1;
			}
			close_image();
			$("#image_submit").show();
			$(".edit_profile_image_upload_loader").hide();
		},
		error:function(){
			$("#image_submit").show();
			$(".edit_profile_image_upload_loader").hide();
		}

		}).attr('action',SAVE_USER_PHOTO).submit();

	});
	$("#save_profile").click(function(e) {
		e.preventDefault();
		submit_profile();
	});
	
	
});
function close_image() {
	$("[id ^='store_images_close_']").click(function() {
		var id_parts = this.id.split("_");
		$("#store_images_div_"+id_parts[id_parts.length - 1]).remove();
	});
}
function fill_store_images() {
	//$(".profile-thumb form .bootstrap-filestyle").html('');
	$.each(merchant_images,function(index,value) {
		$(".profile-thumb form .bootstrap-filestyle").append("<div class='store_imgs_div' id='store_images_div_"+uploaded_images_index+"'><img id='store_images_"+uploaded_images_index+"' src='"+value.url+"' /><a id='store_images_close_"+uploaded_images_index+"'><i class='fa fa-times-circle-o'></i></a><input type='hidden' id='store_images_dimension_div_"+uploaded_images_index+"' value='"+value.width+"_"+value.height+"' /></div>");	
		uploaded_images_index = uploaded_images_index+1;
	});
}
function search_user_name() {
	$("[name='user_name']").change(function() {
		if(this.value == '') {
			chosen_user_name = false;
			return;
		}
		$.ajax({method:'post',url:SEARCH_USER_NAME+this.value,success:function(response) {
			if(response == 'false') {
				chosen_user_name = true;
				$("#chosen_user_name").hide();
			}
			else {
				chosen_user_name = false;	
				$("#chosen_user_name").show();
			}
		}});
	});
}

function fill_states() {
	$("[name='state']").chosen({
      placeholder_text_single: "Choose State..",
      no_results_text: "Oops, nothing found!"
    });
    change_state();
}
function change_state() {
	$("[name='state']").change(function() {
    	fill_cities();
    });
}
function fill_cities() {
	var cities = '<option value=""></option>';
    var selected_state = $("[name='state'] :selected").val();
    $("[name='city']").html('');
    fill_localities();
    $.each(locations.all_cities,function(index,value) {
    	var city_state = value.split("--");
   		if(city_state[1] == selected_state)
   			cities +='<option value="'+city_state[0]+'">'+city_state[0]+'</option>';
    });
    $("[name='city']").html(cities);
    $("[name='city']").chosen({
     	placeholder_text_single: "Choose City..",
      	no_results_text: "Oops, nothing found!"
	});
	$("[name='city']").trigger("chosen:updated");
	change_city();
}

function change_city() {
	$("[name='city']").change(function() {
		fill_localities();
	});
}
function fill_localities() {
   	var localities = '<option value=""></option>';
   	var selected_city = $("[name='city'] :selected").val();
   	$("[name='locality']").html('');
   	$.each(locations.all_localities,function(index,value) {
   		var locality_city = value.split("--");
   		if(locality_city[1] == selected_city)
   			localities +='<option value="'+locality_city[0]+'">'+locality_city[0]+'</option>';
   	});
   	$("[name='locality']").html(localities);
   	$("[name='locality']").chosen({
   		placeholder_text_single: "Choose Locality..",
   		no_results_text: "Oops, nothing found!"
  	});
  	$("[name='locality']").trigger("chosen:updated");
}

function prefill_locations() {
	if(merchant != undefined) {
		if(merchant.state != undefined && merchant.state!='') {
			$("[name='state']").val(merchant.state).trigger("chosen:updated");
			fill_cities();
			if(merchant.city != undefined && merchant.city !='') {
				$("[name='city']").val(merchant.city).trigger("chosen:updated");
				fill_localities();
				if(merchant.locality != undefined && merchant.locality !='') {
					$("[name='locality']").val(merchant.locality).trigger("chosen:updated");
				}
			}
		}
	}
}

function submit_profile() {
	$(".form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			if(chosen_user_name == false)
				return false
			var state = $("[name='state'] :selected").val();
			var city = $("[name='city'] :selected").val();
			var locality = $("[name='locality'] :selected").val();
			if(locality == '' || city =='' || state == '') {
				return false;
			}
			$(".form_buttons").hide();
			var user_images = [];
			$("[id^='store_images_']").each(function(index,value) {
				var id_parts = this.id.split("_");
				var dimension = $("#store_images_dimension_div_"+id_parts[id_parts.length-1]).val();
				console.log(dimension);
				dimension = dimension.split("_");
				user_images.push({url:value.src,width:dimension[0],height:dimension[1]});
			});
			arr.push({'name':'user_images',value:JSON.stringify(user_images)});

		},
		success:function(response){
			response = JSON.parse(response);
			$(".form_buttons").show();
			if(response.status == 'SUCCESS') {
				if(login_statuses.edit_profile == user_status)
					window.location = DASHBOARD_URL;
				else {
					$(".profile_update_success").html(response.message);
					$(".profile_update_success").show();
				}
				window.location.hash = "#profile_alert_messages";
			}
			else {
				$(".profile_update_failure").html(response.message);
				$(".profile_update_failure").show();
				$(".form_buttons").show();
			}

		},
		error:function(){
			$(".profile_update_failure").html('Couldn\'t save profile');
			$(".profile_update_failure").show();
			$(".form_buttons").show();
			window.location.hash = "#profile_alert_messages";
		}
	}).attr('action',SAVE_USER_PROFILE).submit();
}