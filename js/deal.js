var uploaded_images_index = 1;
$(document).ready(function() {

	closeImage();
	fillDealImages();
	enable_deal_form_functions();
	$("#image_submit").click(function(e) {
		e.preventDefault();
		$(".imageForm").ajaxForm({

		beforeSubmit:function(arr, $form, options){
			$("#image_submit").hide();
			$(".edit_profile_image_upload_loader").show();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				$(".profile-thumb form .bootstrap-filestyle").append("<div class='deal_imgs_div' id='deal_images_div_"+uploaded_images_index+"'><img id='deal_images_"+uploaded_images_index+"' src='"+response.image_url+"' /><a id='deal_images_close_"+uploaded_images_index+"'><i class='fa fa-times-circle-o'></i></a><input type='hidden' id='deal_images_dimension_div_"+uploaded_images_index+"' value='"+response.width+"_"+response.height+"' /></div>");
				uploaded_images_index = uploaded_images_index+1;
			}
			closeImage();
			$("#image_submit").show();
			$(".edit_profile_image_upload_loader").hide();
		},
		error:function(){
			$("#image_submit").show();
			$(".edit_profile_image_upload_loader").hide();
		}

		}).attr('action',SAVE_DEAL_PHOTO).submit();
	});
	
	$(":file").filestyle({buttonBefore: true});
	/*$('.spinner .btn:first-of-type').on('click', function() {
		var myin = $(this).parent().parent().find('input:not([type=hidden]):first'); 
    	myin.val(parseInt(myin.val(), 10) + 5);
	  });
  	$('.spinner .btn:last-of-type').on('click', function() {
		var myin = $(this).parent().parent().find('input:not([type=hidden]):first'); 
    	myin.val(parseInt(myin.val(), 10) - 5);
  	});*/
  	$('#productSku').change(function() {
		enableDisableNewProductFields();
	});
	$("#save_deal").click(function(e) {
		e.preventDefault();
		submit_deal();
	});
});
function submit_deal() {
	$(".deal_form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			$("#deal_save_buttons").hide();
			var deal_images = [];
			$("[id^='deal_images_']").each(function(index,value) {
				var id_parts = this.id.split("_");

				var dimension = $("#deal_images_dimension_div_"+id_parts[id_parts.length-1]).val();
				dimension = dimension.split("_");
				deal_images.push({url:value.src,width:dimension[0],height:dimension[1]});
			});
			arr.push({'name':'deal_images',value:JSON.stringify(deal_images)});
			arr.push({'name':'deal_id',value:deal_id});
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				if(editing!='' && editing != false) {
					$(".deal_update_success").html(response.message);
					$(".deal_update_success").show();
					window.location.hash = "#deal_alert_messages";
				}
				else
					window.location = EDIT_DEAL_URL+response.deal_id;
			}
			else {
				$(".deal_update_failure").html(response.message);
				$(".deal_update_failure").show();
				window.location.hash = "#deal_alert_messages";
			}
			$("#deal_save_buttons").show();
		},
		error:function(){
			$("#deal_save_buttons").show();
		}
	}).attr('action',SAVE_DEAL_URL).submit();
}
function enable_deal_form_functions() {
	$("#dtBox").DateTimePicker({
		dateFormat: "MM-dd-yyyy",
		timeFormat: "HH:mm",
		dateTimeFormat: "dd-MM-yyyy hh:mm"
	});
	$('#dealTypeAllStore').change(function() {
		enableDisableDealType();
	});
	$('#dealTypeSku').change(function() {
		enableDisableDealType();
	});
	$('#rewardTypePercentage').change(function() {
		enableDisableRewardType();
	});
	$('#rewardTypeAmount').change(function() {
		enableDisableRewardType();
	});
	$("#rewardPercentage").on("keydown keyup change",function() {
			if(this.value !='') {
				if(this.value.slice("-1") != '%')
					$("#rewardPercentage").val(this.value+'%');
				else if(isNaN(this.value.substring(0, this.value.length - 1))) {
					$("#rewardPercentage").val('0%');
				}

			}
		}
	);
	$("#minimumBillSize").on("keydown keyup change",function() {
		if(this.value !='') {
			if(this.value.substr(0,1) != '₹')
				$("#minimumBillSize").val('₹ '+this.value);
			else if(isNaN(this.value.substring(2, this.value.length))) {
				$("#minimumBillSize").val('₹ 0');
			}
				
		}
	});

	enableDisableRewardType();
	enableDisableDealType();
}

function enableDisableRewardType()
	{
		if ($('#rewardTypePercentage').is(':checked')) {
			$("#rewardAmountHidden").hide();
			$("#rewardPercentageHidden").show();
		}
		if ($('#rewardTypeAmount').is(':checked')) {
			$("#rewardPercentageHidden").hide();
			$("#rewardAmountHidden").show();
		}
	}

function enableDisableDealType()
	{
		if ($('#dealTypeAllStore').is(':checked')) {
			$('#productSku').val('');
			$("#productSku").change();
			$("#dealTypeHidden").hide();
		}
		if ($('#dealTypeSku').is(':checked')) {
			$("#dealTypeHidden").show();
		}
	}
function enableDisableNewProductFields()
	{
		if($('#productSku').val() == 'Add New')
			{
			var productSkuParentParent = $('#productSku').parent().parent();
			productSkuParentParent.append('<div class="form-group"><input type="text" id="productTitle" name="productTitle" class="form-control" value="" placeholder="Product Name"/></div><div class="form-group"><div class="checkbox checkbox-info"><input type="checkbox" id="veg" name="veg" class="form-control" value="true" /><label for="veg">Veg</label></div><div class="checkbox checkbox-info"><input type="checkbox" id="nonVeg" name="nonVeg" class="form-control" value="true" /><label for="nonVeg">Non Veg</label></div></div><div class="form-group"><input type="number" id="productMrp" name="productMrp" class="form-control" placeholder="MRP"/></div>');
			}
		else
			{
				/*$('#productMrp').parent().remove();
				$('#nonVeg').parent().remove();
				$('#veg').parent().remove();*/
				$('#productTitle').parent().parent().remove();
			}
	}

function fillDealImages() {
	//$(".profile-thumb form .bootstrap-filestyle").html('');
	$.each(all_deal_images,function(index,value) {
		$(".profile-thumb form .bootstrap-filestyle").append("<div class='deal_imgs_div' id='deal_images_div_"+uploaded_images_index+"'><img id='deal_images_"+uploaded_images_index+"' src='"+value.imageUrl+"' /><a id='deal_images_close_"+uploaded_images_index+"'><i class='fa fa-times-circle-o'></i></a><input type='hidden' id='deal_images_dimension_div_"+uploaded_images_index+"' value='"+value.imageWidth+"_"+value.imageHeight+"'/></div>");	
		uploaded_images_index = uploaded_images_index+1;
	});
}

function closeImage() {
	$("[id ^='deal_images_close_']").click(function() {
		var id_parts = this.id.split("_");
		$("#deal_images_div_"+id_parts[id_parts.length - 1]).remove();
	});
}

function bill_size_decrement() {
	var bill_size = parseInt($("#minimumBillSize").val().replace(/\D/g,''));
	if(bill_size!='' && bill_size > 0) {
		var new_bill_size = bill_size-1;
		$("#minimumBillSize").val('₹ '+new_bill_size);
	}
	else {
		$("#minimumBillSize").val('₹ 0');
	}
}

function bill_size_increment() {
	var bill_size = parseInt($("#minimumBillSize").val().replace(/\D/g,''));

	if(bill_size > 0 && bill_size!='') {
		var new_bill_size = bill_size+1;
		$("#minimumBillSize").val('₹ '+new_bill_size);
	}
	else if(bill_size == false) {
		$("#minimumBillSize").val('₹ 1');
	}
	else {
		$("#minimumBillSize").val('₹ 0');
	}
}
function bill_discount_decrement() {
	var bill_discount = parseInt($("#rewardPercentage").val().replace(/\D/g,''));
	if(bill_discount!='' && bill_discount > 0) {
		var new_bill_discount = bill_discount-1;
		$("#rewardPercentage").val(new_bill_discount+'%');
	}
	else {
		$("#rewardPercentage").val('0%');
	}
}

function bill_discount_increment() {
	var bill_discount = parseInt($("#rewardPercentage").val().replace(/\D/g,''));
	if(bill_discount!='' && bill_discount >= 0) {
		var new_bill_discount = bill_discount+1;
		$("#rewardPercentage").val(new_bill_discount+'%');
	}
	else if(bill_discount == false) {
		$("#rewardPercentage").val('1%');
	}
	else {
		$("#rewardPercentage").val('0%');
	}
}
function bill_amount_decrement() {
	var bill_amount = parseInt($("#rewardAmount").val().replace(/\D/g,''));
	if(bill_amount!='' && bill_amount > 0) {
		var new_bill_amount = bill_amount-1;
		$("#rewardAmount").val(new_bill_amount);
	}
	else {
		$("#rewardAmount").val('0');
	}
}

function bill_amount_increment() {
	var bill_amount = parseInt($("#rewardAmount").val());
	if(bill_amount!='' && bill_amount >= 0) {
		var new_bill_amount = bill_amount+1;
		$("#rewardAmount").val(new_bill_amount);
	}
	else if(bill_amount == false) {
		$("#rewardAmount").val('1');
	}
	else {
		$("#rewardAmount").val('0');
	}
}
