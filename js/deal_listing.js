var min_scroll = 600;
var listing_period_type = 'today';

var listing_start = 0;
var listing_limit=40;
var last_scroll_no = 0;

var week_listing_start = 0;
var week_listing_limit=40;
var week_last_scroll_no = 0;

var all_listing_start = 0;
var all_listing_limit=40;
var all_last_scroll_no = 0;
var DEAL_IMAGES_URL_PREFIX = "http://magicpin.in/media/catalog/product/";



$(document).ready(function() {
	fill_deals(listing_limit);
	activate_page_links();
	enable_onscroll();
	$(".today_time").click(function() {
		listing_period_type = 'today';
		$("[id^='deal_listing_']").hide();
		$("#deal_listing_today").show();
		$("[id$='_time_button']").removeClass('active');
		$("[id$='today_time_button']").addClass('active');
		if(listing_start == 0)
			fill_deals(listing_limit);
	});

	$(".week_time").click(function() {
		listing_period_type = 'week';
		$("[id^='deal_listing_']").hide();
		$("#deal_listing_week").show();
		$("[id$='_time_button']").removeClass('active');
		$("[id$='week_time_button']").addClass('active');
		if(week_listing_start == 0)
			fill_deals(week_listing_limit);
	});

	$(".all_time").click(function() {
		listing_period_type = 'all';
		$("[id^='deal_listing_']").hide();
		$("#deal_listing_all").show();
		$("[id$='_time_button']").removeClass('active');
		$("[id$='all_time_button']").addClass('active');
		if(all_listing_start == 0)
			fill_deals(all_listing_limit);
	});
});
function enable_onscroll() {
	$(window).scroll(function (event) {
    	var scroll = $(window).scrollTop();
    	var scroll_no =parseInt(scroll/min_scroll);

    	if(listing_period_type == 'today') {
     		if(scroll_no>last_scroll_no) {
    			last_scroll_no = scroll_no;
    			fill_deals(((scroll_no+1) * listing_limit) - listing_start);
    		}
    	}
    	else if(listing_period_type == 'week') {
     		if(scroll_no>week_last_scroll_no) {
    			week_last_scroll_no = scroll_no;
    			fill_deals(((scroll_no+1) * week_listing_limit) - week_listing_start);
    		}
    	}
    	else if(listing_period_type == 'all') {
     		if(scroll_no>all_last_scroll_no) {
    			all_last_scroll_no = scroll_no;
    			fill_deals(((scroll_no+1) * all_listing_limit) - all_listing_start);
    		}
    	}
	});
}

function fill_deals(scroll_limit) {
	if(listing_period_type == 'today')
		start = listing_start;
	else if(listing_period_type == 'week')
		start = week_listing_start;
	else if(listing_period_type == 'all')
		start = all_listing_start;
	else
		start = 0;
	$.ajax({method:'post',url:GET_DEALS_CUSTOMER_URL+listing_period_type+'/'+start+"/"+scroll_limit,success:function(response) {
		var str = "";
		response = JSON.parse(response);
		$.each(response,function(index,value) {
			var sku_parts = value.SKU.trim().split("#");
			var selfie = value.img_url.trim();
			selfie = DEAL_IMAGES_URL_PREFIX+selfie;
			var amount = value.price;
			var locality = value.locality;
			var cashback = (parseFloat(value.cashback_percent)*parseFloat(amount))/100.00;

			str += '<div class="col-md-4 col-sm-8 col-xs-12">';
            str += '<div class="manage-deal-wrapper ">';
			str += '<div class="customer-profile-wrapper">';
			str += '<div class="customer-profile-details">';
			str += '<div class="price-box">';
			str += '<h4>'+sku_parts[0]+'</h4>';
			str += '<div class="price-info">';
			str += '<span class="icon"><i class="fa fa-inr"></i></span>250';
			str += '<span class="cash-back">Cashback</span>';
			str += '</div>';
			str += '</div>';
			str += '<div class="views">';
			//str += '<p><i class="fa fa-eye"></i>View</p>';
			//str += '<div class="view-number">70</div>';
			str += '</div>';
			str += '</div>';
			str += '<a href="#"><img class="img-responsive" src="'+selfie+'"></a>';
			str += '</div>';
			str += '<div class="sales-deatail-block">';
			//str += '<div class="customer-num"><span class="icon"><i class="fa fa-user"></i></span>Customers';
			//str += '<span class="abs-right">10 </span></div>';
			str += '<div class="sales-num">';
			//str += '<span class="icon"><i class="fa fa-inr"></i></span>Sales';
			str += '<span class="abs-right"> </span></div>';
			str += '<div class="fund-num">';
			//str += '<span class="icon"><i class="fa fa-inr"></i></span>Funds';
			str += '<span class="abs-right"> </span></div>';
			str += '</div>';
			str += '<div class="share-wraper ">';
			str += '<div class="row">';
			str += '<div class="col-md-4 col-sm-8 col-xs-12 share-link">';
			str += '<div class="share-block">';
			str += '<span class="icon"><i class="fa fa-share-alt"></i></span>';
			str += '<a >Share</a>';
			str += '</div>';
			str += '</div>';
			str += '<div class="col-md-4 col-sm-8 col-xs-12 share-link">';
			str += '<div class="share-block">';
			str += '<a href="'+base_url+'deal/edit/'+value.product_id+'">Edit</a>';
			str += '</div>';
			str += '</div>';
			str += '<div class="col-md-4 col-sm-8 col-xs-12 share-link">';
			str += '<div class="share-block">';
			str += '<a href="#">End</a>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
		});
		$("#deal_listing_"+listing_period_type+"_loader").hide();
		if(listing_period_type == 'today')
			listing_start = listing_start + scroll_limit;
		else if(listing_period_type == 'week')
			week_listing_start = week_listing_start+scroll_limit;
		else if(listing_period_type == 'all')
			all_listing_start = all_listing_start + scroll_limit
		$(".deal_listing_"+listing_period_type).append(str);
	}
	});
}
