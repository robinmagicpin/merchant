var FIRST_REGISTER_URL = base_url+"login/first_register/";
var VALIDATE_OTP_URL = base_url+"login/first_register_validate_otp/";
var EDIT_PROFILE_URL = base_url+"login/edit_profile/";
var LOGIN_URL =base_url+"login/login_user/";
var LOGOUT_URL =base_url+"login/logout/";

var FORGET_PASSWORD_OTP_VALIDATE_URL = base_url+"login/forget_password_update/";
var SEND_OTP_URL = base_url+"login/forget_password_send_otp/";
var SAVE_USER_PROFILE = base_url+"login/save_profile/";
var SAVE_USER_PHOTO =  base_url+"login/save_photo/";
var SEARCH_USER_NAME = base_url+"login/seach_user_name/";

var DASHBOARD_URL = base_url+"login/dashboard/";
var DASHBOARD_SUMMARY_URL = base_url+"login/dashboard_summary/";
var EDIT_PASSWORD_URL = base_url+"login/change_password/";

var SAVE_DEAL_PHOTO =  base_url+"deal/do_upload/";
var GET_TRANSACTIONS_CUSTOMER_URL = base_url+"customer/get_transactions/";
var GET_TRANSACTIONS_AROUND_YOU_URL = base_url+"customer/get_around_transactions/";

var GET_DEALS_CUSTOMER_URL = base_url+"customer/get_deals/";
var GET_DEALS_AROUND_YOU_URL = base_url+"customer/get_around_deals/";

var SAVE_DEAL_URL = base_url+"deal/post/";
var CREATE_DEAL_URL = base_url+"deal/create/";
var EDIT_DEAL_URL = base_url+"deal/edit/";

$(window).scroll(function() {
    if ($(this).scrollTop() > 1){  
    	$('header').addClass("sticky");
        $('.create-add-btn').addClass("CreateBtn");
    }
    else{
    	$('header').removeClass("sticky");
        $('.create-add-btn').removeClass("CreateBtn");
    }
});
function get_time_transaction_time_from_now(milliseconds) {
	var time_str = "";
	var now = new Date();
	var transaction_time = new Date(milliseconds);
	var seconds = Math.round((now-transaction_time)/1000);
	if(seconds >=60) {
		var mins = seconds/60;
		if(mins>=60) {
			var hours = mins/60;
			if(hours>=24)  {
				var days = hours/24;
				time_str += parseInt(days) + " days ago";
			}
			else
				time_str += parseInt(hours) + " hours ago";
		}
		else
			time_str += parseInt(mins) + " minutes ago";
	}
	else 
		time_str += parseInt(seconds) + " seconds ago";
	return time_str;
}

function activate_page_links () {
	$(".create-add-btn").click(function() {
		window.location = CREATE_DEAL_URL;
	});
	$(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none",
        'z-index':999999,
        'autoSize':false,
        'scrolling':'no',
	});
}