var entered_register_phone_no = false;
var entered_fp_phone_no = false;

$(document).ready(function() {
	enable_first_login();
	$(".become_member").click(function() {
		enable_first_register();
	});
	$(".login_screen").click(function() {
		enable_first_login();
	});
	$(".forget_password").click(function() {
		enable_fp();
	});
	$(".user_name input").focus();
});
function enable_fp() {
	$(".become_member").hide();
	$(".login_screen").hide();
	$(".login_div").hide();
	$(".register_div").hide();
	$(".forget_div").show();

	$(".forget_password_form1").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			if(!$(".forget_password_form1")[0].checkValidity())
				return false;
			var phone = $(".fp_phone_no input").val();
			arr.push({name:'phone', value:phone });
			$(".fp_done1").hide();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				$(".forget_password_form1").hide();
				$(".forget_password_form2").show();
				$(".forget-wrapper-title").html('Please Enter OTP & NEW PASSWORD');
			}
			else {
				$(".fp_done1").show();
				$(".forget-wrapper-title").html(response.message);
			}
		},
		error:function(){
			$(".fp_done1").show();
			$(".forget-wrapper-title").html('Network Error');
		}
	}).attr('action',SEND_OTP_URL).submit();

	$(".forget_password_form2").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			if(!$(".forget_password_form2")[0].checkValidity())
				return false;
			var phone = $(".fp_phone_no input").val();
			var otp = $(".fp_otp input").val();
			var password = $(".fp_new_password input").val();
			var password_confirm = $(".fp_new_password_confirm input").val();
			if(password != password_confirm) {
				$(".forget-wrapper-title").html('Passwords do not match');
				return false;
			}
			arr.push({name:'phone', value:phone });
			arr.push({name:'otp', value:otp });
			arr.push({name:'password', value:password })
			$(".fp_done2").hide();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				enable_first_login();
				$(".login_error").html('password updated successfully');
				$(".forget-wrapper-title").html('');
			}
			else {
				$(".fp_done2").show();
				$(".forget-wrapper-title").html(response.message);
			}
		},
		error:function(){	
			$(".fp_done2").show();
			$(".forget-wrapper-title").html('Network Error');
		} 
	}).attr('action',FORGET_PASSWORD_OTP_VALIDATE_URL).submit();
}
function enable_first_register() {
	$(".become_member").hide();
	$(".login_screen").show();
	$(".login_div").hide();
	$(".register_div").show();
	$(".forget_div").hide();

	$(".register_form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			if(!$(".register_form")[0].checkValidity())
				return false;
			var phone = $(".enter_phone_no input").val();
			arr.push({name:'phone', value:phone });
			$(".enter_phone_otp1").hide();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				entered_register_phone_no = true;
				$(".register_form").hide();
				$(".register_otp_form").show();
				$(".register-wrapper-title").html('Please Enter OTP');
			}
			else {
				$(".enter_phone_otp1").show();
				$(".register-wrapper-title").html(response.message);
			}
		},
		error:function(){
			$(".enter_phone_otp1").show();
			$(".register-wrapper-title").html('Network Error');
		} 
	}).attr('action',FIRST_REGISTER_URL).submit();
	$(".register_otp_form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			if(!$(".register_otp_form")[0].checkValidity())
				return false;
			var phone = $(".enter_phone_no input").val();
			var otp = $(".enter_otp input").val();
			arr.push({name:'phone', value:phone });
			arr.push({name:'otp', value:otp });
			$(".enter_phone_otp2").hide();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') 
				window.location.href = EDIT_PROFILE_URL;
			else {
				$(".enter_phone_otp2").show();
				$(".register-wrapper-title").html(response.message);
			}
		},
		error:function(){
			$(".enter_phone_otp2").show();
			$(".register-wrapper-title").html('Network Error');
		} 
	}).attr('action',VALIDATE_OTP_URL).submit();
}

function enable_first_login() {
	$(".forget_password").show();
	$(".become_member").show();
	$(".login_screen").hide();
	$(".login_div").show();
	$(".register_div").hide();
	$(".forget_div").hide();
	$(".login_form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			if(!$(".login_form")[0].checkValidity())
				return false;
			var user_name = $(".user_name input").val();
			var password = $(".user_password input").val();
			arr.push({name:'user_name', value:user_name });
			arr.push({name:'password', value:password });
			$(".login_me").hide();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				window.location.href = DASHBOARD_URL;
			}
			else {
				$(".login_me").show();
				$(".login_error").html(response.message);
			}
		},
		error:function(){
			$(".login_me").show();
			$(".login_error").html('Network Error');
		} 
	}).attr('action',LOGIN_URL).submit();
}

