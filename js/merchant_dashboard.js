$(document).ready(function() {
	fill_customers();
	fill_around_merchants();
	activate_page_links();
	$("[id$='_time_button']").click(function() {
		var no_of_days = 30;
		$("[id$='_time_button']").removeClass('active');
		var id_parts = this.id.split("_");
		$("#"+id_parts[0]+"_time_button").addClass('active');
		$.ajax({method:'post',url:DASHBOARD_SUMMARY_URL+id_parts[0],success:function(response) {
			response = JSON.parse(response);
			$(".transaction-count").html(response.transactionCount);
			$(".transaction-sales").html('₹'+response.totalSales);
			$(".transaction-funds").html('₹'+response.funding);
			$(".transaction-balance").html('₹0');
		}})
	});
});


function fill_customers() {
	$.ajax({method:'post',url:GET_TRANSACTIONS_CUSTOMER_URL+'all/0/50',success:function(response) {
		response = JSON.parse(response);
		var str = "";
		var today = new Date();
		today = [today.getFullYear(),today.getMonth()+1,
               today.getDate()].join('-')+' '+
              [today.getHours(),
               today.getMinutes(),
               today.getSeconds()].join(':');
        today = moment(today,"YYYY-MM-DD HH:mm:ss");
		$.each(response,function(index,value) {
			var txn_date_arr = value.txn_time.split("T");
			var txn_time_arr = txn_date_arr[1].split("Z");
			value.txn_time = txn_date_arr[0]+" "+txn_time_arr[0];
			//
			var time_str = moment.duration(today.diff(value.txn_time,"YYYY-MM-DD HH:mm:ss")).subtract({ hours: "5", minutes: "30"}).humanize() +' ago';
			if(time_str == 'a day ago') {
				time_str = 'Yesterday';
			}
			var sku_parts = value.sku.trim().split("#");
			var selfie = value.selfie_url.trim();
			var selfie_parts = selfie.split("/");
			if(selfie_parts[0] !='http' && selfie_parts[0]!='https') 
				selfie = "http://"+selfie;
			var amount = value.bill_size;
			if(index == 0)
				str += '<div class="item active">';
			else 
				str += '<div class="item">';
			
			str += '<div class="col-md-3 col-sm-6 col-xs-12"><div class="customer-profile-wrapper"><div class="customer-profile-details"><div class="price-info"><span class="icon"><i class="fa fa-inr"></i></span>'+amount+'</div></div><a href="'+selfie+'" class="fancybox"><img src="'+selfie+'" class="img-responsive"></a></div>';
			str += '<div class="time-info"><span class="icon"><i class=" fa fa-clock-o"></i></span> '+time_str+' <span class="attention-msg danger">Report!</span></div>';
			str += '</div>';
			str += '</div>';
		});
		$(".customers_list").html(str);
		move_customer_carousel();
	}
	});
}
function fill_around_merchants() {

	$.ajax({method:'post',url:GET_TRANSACTIONS_AROUND_YOU_URL,success:function(response) {
		var str = "";
		response = JSON.parse(response);
		var today = new Date();
		today = [today.getFullYear(),today.getMonth()+1,
               today.getDate()].join('-')+' '+
              [today.getHours(),
               today.getMinutes(),
               today.getSeconds()].join(':');
       	today = moment(today,"YYYY-MM-DD HH:mm:ss");
		$.each(response,function(index,value) {
			var txn_date_arr = value.txn_time.split("T");
			var txn_time_arr = txn_date_arr[1].split("Z");
			value.txn_time = txn_date_arr[0]+" "+txn_time_arr[0];
			var time_str = moment.duration(today.diff(value.txn_time,"YYYY-MM-DD HH:mm:ss")).subtract({ hours: "5", minutes: "30"}).humanize() +' ago';
			if(time_str == 'a day ago') {
				time_str = 'Yesterday';
			}
			var sku_parts = value.sku.trim().split("#");
			var selfie = value.selfie_url.trim();
			var selfie_parts = selfie.split("/");
			if(selfie_parts[0] !='http' && selfie_parts[0]!='https') 
				selfie = "http://"+selfie;
			var amount = value.price;
			var distance = value.dist.toFixed(2);
			
			if(index == 0)
				str += '<div class="item active">';
			else 
				str += '<div class="item">';
			str += '<div class="col-md-3 col-sm-6 col-xs-12"><div class="customer-profile-wrapper"><div class="customer-profile-details"><h4>'+value.merchant_name+'</h4><div class="price-info"><span class="icon"><i class="fa fa-inr"></i></span>'+amount+'</div></div><a href="'+selfie+'" class="fancybox"><img  src="'+selfie+'" class="img-responsive"></a></div>';
			str += '<div class="time-info"><span class="icon"></span> '+time_str+' </div>';
			str += '</div>';
			str += '</div>';
		});
		$(".around_you").html(str);
		move_customer_carousel_around();
	}
	});
}


function move_customer_carousel() {
	//for carousel
	$('#myCarousel[data-type="multi"] .item').each(function(){
		var next = $(this).next();
  		if (!next.length) {
    		next = $(this).siblings(':first');
  		}
  		next.children(':first-child').clone().appendTo($(this));
	  
	  	for (var i=0;i<2;i++) {
    		next=next.next();
    		if (!next.length) {
	    		next = $(this).siblings(':first');
  			}
    		next.children(':first-child').clone().appendTo($(this));
		}
	});
}
function move_customer_carousel_around() {
	//for carousel
	$('#myCarousel1[data-type="multi"] .item').each(function(){
		var next = $(this).next();
  		if (!next.length) {
    		next = $(this).siblings(':first');
  	}
  		next.children(':first-child').clone().appendTo($(this));
  
  		for (var i=0;i<2;i++) {
    		next=next.next();
    		if (!next.length) {
    			next = $(this).siblings(':first');
  			}
    		next.children(':first-child').clone().appendTo($(this));
	  	}
	});
}

