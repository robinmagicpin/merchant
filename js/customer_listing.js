var min_scroll = 600;
var listing_period_type = 'today';

var listing_start = 0;
var listing_limit=40;
var last_scroll_no = 0;

var week_listing_start = 0;
var week_listing_limit=40;
var week_last_scroll_no = 0;

var all_listing_start = 0;
var all_listing_limit=40;
var all_last_scroll_no = 0;

var generic_listing_start = 0;
var generic_listing_limit=40;
var generic_last_scroll_no = 0;

var last_start_date = '';
var last_end_date = '';


$(document).ready(function() {
	fill_customers(listing_limit);
	activate_page_links();
	enable_onscroll();
	$(".today_time").click(function() {
		listing_period_type = 'today';
		$("[id^='customer_listing_']").hide();
		$(".customer_listing_today").show();
		$("[id$='_time_button']").removeClass('active');
		$("#today_time_button").addClass('active');
		if(listing_start == 0)
			fill_customers(listing_limit);
	});

	$(".week_time").click(function() {
		listing_period_type = 'week';
		$("[id^='customer_listing_']").hide();
		$(".customer_listing_week").show();
		$("[id$='_time_button']").removeClass('active');
		$("#week_time_button").addClass('active');
		if(week_listing_start == 0)
			fill_customers(week_listing_limit);
	});

	$(".all_time").click(function() {
		listing_period_type = 'all';
		$("[id^='customer_listing_']").hide();
		$(".customer_listing_all").show();
		$("[id$='_time_button']").removeClass('active');
		$("#all_time_button").addClass('active');
		if(all_listing_start == 0)
			fill_customers(all_listing_limit);
	});
	$("#dtBox").DateTimePicker({
		dateFormat: "MM-dd-yyyy",
		dateTimeFormat: "yyyy-MM-dd"
	});
	$("[id$='_date']").change(function() {
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
		if((start_date=='' || end_date == '' || (start_date == last_start_date) && (last_end_date==end_date))) {
			return;
		}
		listing_period_type = 'generic'; 
		$("[id^='customer_listing_']").hide();
		$(".customer_listing_generic").show();
		$("[id$='_time_button']").removeClass('active');
		last_start_date = start_date;
		last_end_date = end_date;

		fill_customers(generic_listing_limit,start_date,end_date);
	});
});
function enable_onscroll() {
	$(window).scroll(function (event) {
    	var scroll = $(window).scrollTop();
    	var scroll_no =parseInt(scroll/min_scroll);

    	if(listing_period_type == 'today') {
     		if(scroll_no>last_scroll_no) {
    			last_scroll_no = scroll_no;
    			fill_customers(((scroll_no+1) * listing_limit) - listing_start);
    		}
    	}
    	else if(listing_period_type == 'week') {
     		if(scroll_no>week_last_scroll_no) {
    			week_last_scroll_no = scroll_no;
    			fill_customers(((scroll_no+1) * week_listing_limit) - week_listing_start);
    		}
    	}
    	else if(listing_period_type == 'all') {
     		if(scroll_no>all_last_scroll_no) {
    			all_last_scroll_no = scroll_no;
    			fill_customers(((scroll_no+1) * all_listing_limit) - all_listing_start);
    		}
    	}
    	else {
     		if(scroll_no>generic_last_scroll_no) {
    			generic_last_scroll_no = scroll_no;
    			var start_date = $("#start_date").val();
				var end_date = $("#end_date").val();
    			fill_customers(((scroll_no+1) * generic_listing_limit) - generic_listing_start,start_date,end_date);
    		}
    	}
	});
}
function fill_customers(scroll_limit,start_date,end_date) {	

	start_date = typeof start_date !== 'undefined' ? start_date : '';
	end_date = typeof end_date !== 'undefined' ? end_date : '';

	if(listing_period_type == 'today')
		start = listing_start;
	else if(listing_period_type == 'week')
		start = week_listing_start;
	else if(listing_period_type == 'all')
		start = all_listing_start;
	else if(listing_period_type == 'generic')
		start = generic_listing_start;
	else
		start = 0;
	$.ajax({method:'post',url:GET_TRANSACTIONS_CUSTOMER_URL+listing_period_type+'/'+start+"/"+scroll_limit+"/"+start_date+"/"+end_date,success:function(response) {
		response = JSON.parse(response);
		var str = "";
		var today = new Date();
		today = [today.getFullYear(),today.getMonth()+1,
               today.getDate()].join('-')+' '+
              [today.getHours(),
               today.getMinutes(),
               today.getSeconds()].join(':');
        today = moment(today,"YYYY-MM-DD HH:mm:ss");
        
		$.each(response,function(index,value) {
			var txn_date_arr = value.txn_time.split("T");
			var txn_time_arr = txn_date_arr[1].split("Z");
			value.txn_time = txn_date_arr[0]+" "+txn_time_arr[0];
			var time_str = moment.duration(today.diff(value.txn_time,"YYYY-MM-DD HH:mm:ss")).subtract({ hours: "5", minutes: "30"}).humanize() +' ago';
			if(time_str == 'a day ago') {
				time_str = 'Yesterday';
			}
			var sku_parts = value.sku.trim().split("#");
			var selfie = value.selfie_url.trim();
			var selfie_parts = selfie.split("/");
			if(selfie_parts[0] !='http' && selfie_parts[0]!='https') 
				selfie = "http://"+selfie;
			var amount = value.bill_size;

			str += '<div class="col-md-3 col-sm-6 col-xs-12 customer-list-item">';
			str += '<div class="customer-profile-wrapper">';
			str += '<div class="customer-profile-details">';
			//str += '<h4>'+sku_parts[0]+'</h4>';
			str += '<div class="price-info">';
			str += '<span class="icon"><i class="fa fa-inr"></i></span>'+amount;
			str += '</div></div>';
			str += '<a href="'+selfie+'" class="fancybox"><img class="img-responsive" src="'+selfie+'"></a>';
			str += '</div>';
			str += '<div class="time-info">';
			str += '<span class="icon"><i class="fa fa-clock-o"></i></span>'+time_str;
			str += '<span class="attention-msg danger">Report!</span>';
			str += '</div>';
			str += '</div>';
		});
		$("#customer_listing_"+listing_period_type+"_loader").hide();
		$(".customer_listing_"+listing_period_type).append(str);
		if(listing_period_type == 'today')
			listing_start = listing_start + scroll_limit;
		else if(listing_period_type == 'week')
			week_listing_start = week_listing_start+scroll_limit;
		else if(listing_period_type == 'all')
			all_listing_start = all_listing_start + scroll_limit;
		else
			generic_listing_start = generic_listing_start + scroll_limit;
	}
	});
}



