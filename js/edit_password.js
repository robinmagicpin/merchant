$(document).ready(function() {
	$("#save_password").click(function(e) {
		e.preventDefault();
		$("#reset_password_form").ajaxForm({
		beforeSubmit:function(arr, $form, options){
			var new_password = $("[name='new_password']").val();
			var confirm_password = $("[name='confirm_password']").val();
			if(new_password!=confirm_password) {
				$(".profile_update_failure").html('Passwords do not match');
				$(".profile_update_failure").show();
				return false;
			}
			$(".form_buttons").hide();
		},
		success:function(response){
			response = JSON.parse(response);
			if(response.status == 'SUCCESS') {
				$(".profile_update_failure").hide();
				$(".profile_update_success").html(response.message);
				$(".profile_update_success").show();
				setTimeout(login_again, 1000);
			}
			else {
				$(".profile_update_success").hide();
				$(".profile_update_failure").html(response.message);
				$(".profile_update_failure").show();
			}
			$(".form_buttons").show();
		},
		error:function(){
			$(".form_buttons").show();	
		}

		}).attr('action',EDIT_PASSWORD_URL).submit();

	});
	
});

function login_again() {
	window.location.href= LOGOUT_URL;
}