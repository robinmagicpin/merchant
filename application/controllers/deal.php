<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deal extends CI_Controller {
	private $user_data;
	public function __construct() {
		parent::__construct();
		$this->load->helper('common_helper');
		$this->load->library('session');
		$this->load->model('Deal_model');
	}
	private function check_login() {
		global $login_statuses;
		$this->user_data = $this->session->all_userdata();
		if(isset($this->user_data['status']) && $this->user_data['status'] == $login_statuses['dashboard'])
			return true;
		return false;
	}

	public function _remap($method,$params = array())
	{
		if(!$this->check_login()) {
			header("Location:".base_url."login/index");
			die;
		}
		else if(method_exists($this, $method)) {
			$data['user'] = $this->user_data; 
			if($method == 'index')
				$data['deal_list_active'] = true;
			$data['current_method'] = $method;
			$this->load->view('header',$data);
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
	}

	public function index() 
	{
		$this->load->model('Deal_model');
		if(!($list = $this->Deal_model->listAllDeals($this->user_data['merchant']['merchantId']))) {
			$this->session->set_flashdata('flash_message', 'Unable to process the request');
		}
		$this->data['list'] = $list;
		$this->data['user'] = $this->user_data;
		$this->load->view('deal/list',$this->data);
	}
	
	public function create()
	{
		if(isset($this->user_data['all_store_deal']) && $this->user_data['all_store_deal'] >0) {
			header('Location:'.$this->config->config['base_url'].'deal/edit/'.$this->user_data['all_store_deal']);
		}	
		$this->load->view('deal/create');
	}
	public function delete($id=false)
	{
		$this->load->model('Deal_model');
		if(!($response = $this->Deal_model->deleteDeal($id))) {
			$this->session->set_flashdata('flash_message', 'Unable to process the request');
			header('Location:'.$this->config->config['base_url'].'deal/edit/'.$id);	
		}
		$this->session->set_flashdata('flash_message', $response['message']);
		header('Location:'.$this->config->config['base_url'].'deal/index/');
	}
	public function edit($id=false)
	{
		$this->load->model('Deal_model');
		/*if(!$response = $this->Deal_model->listAllProducts(self::$merchantId))
			$this->session->set_flashdata('flash_message', "Could not load product data! Please refresh the page.");
		if($response['count'] < 1)
			$this->session->set_flashdata('flash_message', "No existing product found.");
		$productAutofillData = array();
		$results = array();
		if($response && is_array($response) && array_key_exists('results', $response))
			{
				$results = $response['results'];
			}
		foreach($results as $product)
			{
				$productAutofillData[] = array('label'=>$product['title'],'value'=>$product['productId']);
			}
		$this->data['productAutofillData'] = json_encode($productAutofillData);
		*/
		$response = $this->Deal_model->getDeal($this->user_data['user_id'],$id);
		$data = [];
		$data['status'] = $response['status'];
		if(!$response || !array_key_exists('status', $response) || $response['status'] != 'SUCCESS'){
			header('Location:'.$this->config->config['base_url'].'deal/create/');
		}
		else if(is_array($response) && !array_key_exists('deal', $response)){
			header('Location:'.$this->config->config['base_url'].'deal/create/');
		}
		
		if(is_array($response) && !array_key_exists('product', $response)){
			header('Location:'.$this->config->config['base_url'].'deal/create/');
		}
		if(is_array($response) && !array_key_exists('images', $response))
			$data['message'] = "No image data found.";
		
		if($data['status'] == 'SUCCESS') 
			$data['deal_data'] = generateEditDealFormData($response,$this->user_data['user_id'],$this->user_data['merchant']['merchantId'],'LOL');
		$this->load->view('deal/create',$data);
	}

	public function post($dealId = false)  {
		$formData = $this->input->post();
		if($formData)
			{		
				$response = $this->Deal_model->saveDeal($this->user_data['user_id'],$this->user_data['merchant']['merchantId'],$formData,$formData['deal_id']);
				$status = $response['status'];
				$message = $response['message'];
			}
		else {
			$status = 'FAILURE';
			$message = "Some Error Occurred";
		} 
			
		
		$deal_id = 0;
		if($status == 'SUCCESS') {
			$deal_id = $response['deal']['dealId'];
			$message = "Thanks for submitting the changes. Our team is reviewing the changes, this may take sometime. We will notify you when your changes go live. For any query please write to care@magicpin.in";
		}
		echo json_encode(array('status'=>$status,'message'=>$message,'deal_id'=>$deal_id));
		die;
	}


	public function do_upload($fieldName='dealImage')
	{
		$image = $_FILES;
		$status = 'SUCCESS';
		$image_url = "";
		if(sizeof($image['dealImage']) > 0) {
			$image_name = get_random_string(10);
			$ext = end(explode(".", $image['dealImage']['name']));
			move_uploaded_file($image['dealImage']['tmp_name'],deal_image_path.$image_name.".".$ext);
			list($width, $height) = getimagesize(deal_image_path.$image_name.".".$ext);
			$image_url = deal_image_url.$image_name.".".$ext;
		}
		echo json_encode(array('status'=>$status,'image_url'=>$image_url,'width'=>$width,'height'=>$height));
		die;
		/*
		$merchantId = self::$merchantId;
		$uploadPath = FCPATH.'images/'.date("Y-m-d",time());
		if (!is_dir($uploadPath))
	    {
	        mkdir($uploadPath, 0777, true);
	    }
	    $config['file_name'] = $merchantId.'-'.date("d-m-Y-H:i:s",time());
		$config['upload_path'] = $uploadPath;
		$config['allowed_types'] = 'gif|jpg|png|webp';
		$config['max_size']	= '10240';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload($fieldName))
		{
			$error = array('error' => $this->upload->display_errors());
			$responseArray = array();
			$responseArray['status'] = 'FAILURE';
			$responseArray['error'] = $error['error'];
			echo json_encode($responseArray);
			exit;
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$responseArray = array();
			$uploadImage = array(true, $data);
			$uploadData = $uploadImage[1]['upload_data'];
			$imageUrl = base_url(substr($uploadData['full_path'],strlen(FCPATH)));
			$imageWidth = $uploadData['image_width'];
			$imageHeight = $uploadData['image_height'];
			$imageName = $uploadData['file_name'];
			$responseArray['status'] = 'SUCCESS';
			$responseArray['imageUrl'] = $imageUrl;
			$responseArray['imageWidth'] = $imageWidth;
			$responseArray['imageHeight'] = $imageHeight;
			$responseArray['imageName'] = $imageName;
			echo json_encode($responseArray);
			exit;
		}*/
	}
}

