<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store extends CI_Controller {
	private $user_data;
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('common_helper');
	}
	private function set_user_session($data) {
		$this->session->set_userdata($data);
	}
	private function check_login() {
		global $login_statuses;
		$this->user_data = $this->session->all_userdata();
		if(isset($this->user_data) && isset($this->user_data['status']) && $this->user_data['status'] == $login_statuses['dashboard'])
			return true;
		return false;
	}

	public function _remap($method,$params = array())
	{
		if(!$this->check_login()) {
			header("Location:".base_url."login/index");
			die;
		}
		else if(method_exists($this, $method)) {
			$data['user'] = $this->user_data;
			if($method == 'index')
				$data['store_active'] = true;
			elseif($method == 'around_you')
				$data['around_you_list_active'] = true;
			$data['current_method'] = $method;
			$this->load->view('header',$data);
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
	}

	public function index()
	{	
		global $login_statuses;
		$data['user'] = $this->user_data;
		$this->load->view('store/index',$data);
	}
	public function around_you()
	{	
		$data['user'] = $this->user_data;
		$this->load->view('store/around_you',$data);
	}

}

