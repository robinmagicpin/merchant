<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer extends CI_Controller {
	private $user_data;
	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->helper('common_helper');
		$this->load->model('customer_model');
	}
	private function check_login() {
		global $login_statuses;
		$this->user_data = $this->session->all_userdata();
		if(isset($this->user_data) && isset($this->user_data['status']) && $this->user_data['status'] == $login_statuses['dashboard'])
			return true;
		return false;
	}

	public function _remap($method,$params = array())
	{
		if(!$this->check_login()) {
			header("Location:".base_url."login/index");
			die;
		}
		else if(method_exists($this, $method)) {
			$data['user'] = $this->user_data;
			if($method == 'index')
				$data['customer_list_active'] = true;
			$data['current_method'] = $method;
			$this->load->view('header',$data);
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
	}

	public function index()
	{	
		$this->user_data = $this->session->all_userdata();
		$customer_listing_data['user'] = $this->user_data;
		$this->load->view('customer/list',$customer_listing_data);
	}
	
	public function get_transactions($period_type='all',$start=0,$limit = 40,$start_date='',$end_date='') {
		$response = $this->customer_model->get_transactions($period_type,$start,$limit,$start_date,$end_date);
		echo json_encode($response);die;
	}
	public function get_around_transactions($start=0,$limit = 40) {
		$response = $this->customer_model->get_around_transactions($start,$limit);
		echo json_encode($response);die;
	}
	public function get_deals($period_type='all',$start=0,$limit = 40) {
		$period_type='all';
		$response = $this->customer_model->get_deals($period_type,$start,$limit);
		echo json_encode($response);die;
	}
	public function get_around_deals($start=0,$limit = 40) {
		$response = $this->customer_model->get_around_deals($start,$limit);
		echo json_encode($response);die;
	}
}

