<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	private $user_data;

	public function __construct() {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('login_model');
		$this->load->helper('common_helper');
	}
	private function check_phone_exists($phone_no){
		return false;
	}
	private function set_user_session($data) {
		if(!isset($data['all_store_deal'])) {
			$data['all_store_deal'] = $this->login_model->get_other_deal_id($data['merchant']['merchantId']);
		}
		$this->session->set_userdata($data);
	}
	private function check_login() {
		$this->user_data = $this->session->all_userdata();
		if(isset($this->user_data) && isset($this->user_data['status']))
			return true;
		return false;
	}

	public function _remap($method,$params = array())
	{
		$is_logged_in = $this->check_login();
		$logout_functions = array('index','login_user','first_register','first_register_validate_otp','forget_password_send_otp','forget_password_update');
		if(!in_array($method,$logout_functions) && !$is_logged_in) {
			header("Location:".base_url."login/index");
			die;
		}
		else if(method_exists($this, $method)) {
			$data['user'] = $this->user_data;
			if($method == 'dashboard')
				$data['dashboard_active'] = true;
			$data['current_method'] = $method;
			$this->load->view('header',$data);
            return call_user_func_array(array($this, $method), $params);
        }
        show_404();
	}

	public function index()
	{	
		global $login_statuses;
		if(isset($this->user_data['status'])) {
			if($this->user_data['status'] == $login_statuses['edit_profile']) {
				header("Location:".base_url."login/edit_profile");
				die;
			}
			else if($this->user_data['status'] == $login_statuses['dashboard']) {
				header("Location:".base_url."login/dashboard");
				die;
			}
		}
		$this->load->view('login/login_register');
	}
	public function login_user() {
		global $login_statuses;
		$status = 'SUCCESS';
		$message = 'Some Error Occured';
		$user_name = $this->input->post('user_name');
		$password = $this->input->post('password');
		if($user_name && $password) {
			if(is_numeric($user_name)) {
				$phone_arr = validate_phone_no($user_name);
				$user_name = $phone_arr['phone_no'];
			}
			$response = $this->login_model->login_user($user_name,$password);
			$status = $response['status'];
			$message = $response['message'];
			if($status == 'SUCCESS') {
				$data = array('user_name' => $user_name,'status'=>$login_statuses['dashboard'],'user_id'=>$response['id']); 
				$merchant_data = $this->login_model->get_merchant_data($response['id']);
				$data['merchant'] = $merchant_data['merchant'];
				$data['user_name'] = $merchant_data['user']['userName'];
				$data['phone_no'] = $merchant_data['user']['otpMobile'];
				$data['store_images'] = $merchant_data['images'];
				$this->set_user_session($data);
			}
		}
		else {
			$status = 'FAILURE';
		}
		echo json_encode(array('status'=>$status,'message'=>$message));
		die;
	}
	public function first_register() {
		$phone = $this->input->post('phone');
		$status = 'SUCCESS';
		$message = 'Some Error Occured';
		if($phone) {
			$phone_arr = validate_phone_no($phone);
			if($phone_arr['status'] == 'SUCCESS') {
				$phone_no = $phone_arr['phone_no'];
				$response = $this->login_model->first_register($phone_no);
				$status = $response['status'];
				$message = $response['message'];
			}
			else {
				$status = 'FAILURE';
			}
		}
		else {
			$status = 'FAILURE';
		}
		echo json_encode(array('status'=>$status,'message'=>$message));
		die;
	}
	public function first_register_validate_otp() {
		global $login_statuses;
		$status = 'SUCCESS';
		$message = 'Some Error Occured';
		$phone = $this->input->post('phone');
		$otp = $this->input->post('otp');
		if($phone && $otp) {
			$phone_arr = validate_phone_no($phone);
			if($phone_arr['status'] == 'SUCCESS') {
				$phone_no = $phone_arr['phone_no'];
				$response = $this->login_model->first_register_validate_otp($phone_no,$otp);
				$status = $response['status'];
				$message = $response['message'];
				if($status == 'SUCCESS') {
					$user_id = $response['id'];
					$data = array('status'=>$login_statuses['edit_profile'],'phone_no'=>$phone_no,'user_id'=>$user_id);
					$this->set_user_session($data);
				}
			}
			else {
				$status = 'FAILURE';
			}
		}
		else {
			$status = 'FAILURE';
		}
		echo json_encode(array('status'=>$status,'message'=>$message));
		die;
	}
	public function forget_password_send_otp() {
		$status = 'SUCCESS';
		$message = "Some Error Occured";
		$phone = $this->input->post('phone');
		if($phone) {
			$phone_arr = validate_phone_no($phone);
			if($phone_arr['status'] == 'SUCCESS') {
				$phone = $phone_arr['phone_no'];
				$user_exists = $this->login_model->check_user_exists($phone);
				if(!$user_exists) {
					$status = 'FAILURE';
					$message = 'User Does not Exist.';
				}
				else {
					$response = $this->login_model->send_otp($phone);
					$status = $response['status'];
					$message = $response['message'];
				}
			}
			else {
				$status = 'FAILURE';
			}
		}
		else {
			$status = 'FAILURE';
		}
		echo json_encode(array('status'=>$status,'message'=>$message));
		die;
	}
	public function forget_password_update() {
		$status = 'SUCCESS';
		$message = "Some Error Occured";
		$phone = $this->input->post('phone');
		$otp = $this->input->post('otp');
		$password = $this->input->post('password');
		if($phone && $otp && $password) {
			$phone_arr = validate_phone_no($phone);
			if($phone_arr['status'] == 'SUCCESS') {
				$phone_no = $phone_arr['phone_no'];
				$response = $this->login_model->validate_otp($phone_no,$otp);
				$status = $response['status'];
				$message = $response['message'];
				if($status == 'SUCCESS') {
					$response = $this->login_model->update_password($phone_no,$password);
					$status = $response['status'];
					$message = $response['message'];
				}
			}
		}
		else {
			$status = 'FAILURE';
		}
		echo json_encode(array('status'=>$status,'message'=>$message));
		die;
	}
	public function edit_profile() {
		global $login_statuses;
		$all_data['user'] = $this->user_data;
		$all_data['locations'] = raw_localities();
		$this->load->view('login/edit_profile',$all_data);
	}
	public function edit_password() {
		$all_data['user'] = $this->user_data;
		$this->load->view('login/edit_password',$all_data);	
	}
	public function change_password() {
		$status = 'SUCCESS';
		$message = "Some Error Occured";
		$current_password = $this->input->post('current_password');
		$new_password = $this->input->post('new_password');
		if($new_password) {
			$response = $this->login_model->edit_password($this->user_data['phone_no'],$new_password);
			$status = $response['status'];
			$message = $response['message'];
		}
		else {
			$status = 'FAILURE';
		}
		echo json_encode(array('status'=>$status,'message'=>$message));die;
	}
	public function save_photo() {
		$images = $_FILES;
		$status = 'SUCCESS';
		$image_url = "";
		if(sizeof($images['store_image']) > 0) {
			$image_name = get_random_string(10);
			$ext = end(explode(".", $images['store_image']['name']));
			move_uploaded_file($images['store_image']['tmp_name'],base_image_path.$image_name.".".$ext);
			list($width, $height) = getimagesize(base_image_path.$image_name.".".$ext);
			$image_url = base_image_url.$image_name.".".$ext;
		}
		echo json_encode(array('status'=>$status,'image_url'=>$image_url,'width'=>$width,'height'=>$height));
		die;
	}
	public function save_profile() {
		global $login_statuses;
		$status = 'SUCCESS';
		$message = "Some Error Occured";
		$user_name = $this->input->post('user_name');
		$password = $this->input->post('password');
		$merchant_all_data = $this->prepare_merchant_data();
		$merchant_data = $merchant_all_data['merchant'];
		$merchant_data['categoryId']=1;
		$response = $this->login_model->edit_merchant($merchant_data,$merchant_all_data['images']);
		$status = $response['status'];
		$message = $response['message'];
		if($status == 'SUCCESS') {
			$this->user_data['merchant'] = $response['merchant'];
			$this->user_data['store_images'] = $response['images'];
			$this->set_user_session($this->user_data);
		}
		if($status=='SUCCESS' && ($this->user_data['status'] == $login_statuses['edit_profile'])) {
			$response = $this->login_model->update_user_name($user_name);
			$status = $response['status'];
			$message = $response['message'];
			if($status == 'SUCCESS') {
				$this->user_data['user_name'] = $user_name;
				$this->set_user_session($this->user_data);
			}
		}
		if($status=='SUCCESS' && !empty($password)) {
			$response = $this->login_model->edit_password($this->user_data['phone_no'],$password);
			$status = $response['status'];
			$message = $response['message'];
		}
		if($status == 'SUCCESS') {
			$this->user_data['status'] = $login_statuses['dashboard'];
			$this->set_user_session($this->user_data);
		}
		echo json_encode(array('status'=>$status,'message'=>$message));
		die;
	}
	private function prepare_merchant_data() {
		$merchant_data =[];
		$merchant_data['merchantName'] = $this->input->post('merchantName');
		$merchant_data['addressLine1'] = $this->input->post('addressLine1');
		$merchant_data['locality'] = $this->input->post('locality');
		$merchant_data['state'] = $this->input->post('state');
		$merchant_data['city'] = $this->input->post('city');
		$merchant_data['contactPerson'] = $this->input->post('contactPerson');
		$merchant_data['landlineNumber'] = $this->input->post('landlineNumber');
		if(isset($this->user_data['merchant']['merchantId']))
			$merchant_data['merchantId'] = $this->user_data['merchant']['merchantId'];
		$merchant_images = [];
		$user_images = $this->input->post('user_images');
		$all_user_images = [];
		if(gettype($user_images) == 'string')
			$user_images = json_decode($user_images,true);
		foreach ($user_images as $value) {
			if(isset($value['url']) && !empty($value['url']) )
				$all_user_images[] = $value;
		}
		return array('merchant'=>$merchant_data,'images'=>$all_user_images);
	}

	public function dashboard() {
		$dashboard_data['dashboard_summary'] = $this->login_model->dashboard_summary(30);
		$dashboard_data['user'] = $this->user_data;
		$this->load->view('login/dashboard',$dashboard_data);
	}
	public function dashboard_summary($period_type) {
		$no_of_days = 30;
		if($period_type == 'month') 
			$no_of_days = 30;
		elseif($period_type == 'week') 
			$no_of_days = 7;
		elseif($period_type == 'all') 
			$no_of_days = 2500;
		
		$summary = $this->login_model->dashboard_summary($no_of_days);
		echo json_encode($summary);die;
	}
	public function logout() {
		$this->session->sess_destroy();
		header("Location:".base_url."login");
		die;
	}
	public function seach_user_name($user_name) {
		$response = $this->login_model->check_user_exists($user_name);
		echo ($response == true)?'true':'false';
		die;
	}
	
}

