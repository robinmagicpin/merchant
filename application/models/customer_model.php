<?php
class Customer_model extends CI_Model {
        private $user;
        public function __construct()
        {	
            //$this->load->database();
            $this->load->library('session');
            $this->user = $this->session->all_userdata(); 
        }

        public function get_transactions($period_type,$start,$limit,$start_date,$end_date) {
            $period_str = get_period_dates($period_type);
            $merchant_id = $this->user['merchant']['merchantId'];
            if($period_type == 'generic')
                $transactions_url = $this->config->config['MERCHANT_TRANSACTIONS']."?q=merchant_id:".$merchant_id."&fq=bill_url:*&fq=selfie_time:[".$start_date."T00:00:00Z%20TO%20".$end_date."T00:00:00Z]&fl=*&wt=json&indent=true&sort=selfie_time%20desc&start=".$start."&rows=".$limit;
            else
                $transactions_url = $this->config->config['MERCHANT_TRANSACTIONS']."?q=merchant_id:".$merchant_id."&fq=bill_url:*&fq=selfie_time:[NOW-".$period_str."%20TO%20NOW]&fl=*&wt=json&indent=true&sort=selfie_time%20desc&start=".$start."&rows=".$limit;
            $response = call_api($transactions_url);
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
                $response['body'] = $response['body']['response']['docs'];
            }
            else
                $response['body'] = [];
            return $response['body'];
        }

        public function get_deals($period_type,$start,$limit) {
            $period_str = get_period_dates($period_type);
            $merchant_id = $this->user['merchant']['merchantId'];
            $transactions_url = $this->config->config['MERCHANT_DEALS']."?q=merchant_id:".$merchant_id."&fl=*&wt=json&rows=".$limit."&start=".$start;
            $response = call_api($transactions_url);
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
                $response['body'] = $response['body']['response']['docs'];
            }
            else
                $response['body'] = [];
            return $response['body'];
        }

        public function get_around_transactions($start,$limit) {
            $merchant_id = $this->user['merchant']['merchantId'];
            if(!empty($this->user['merchant']['lat']) && !empty($this->user['merchant']['lon'])) {
                $transactions_url = $this->config->config['MERCHANT_TRANSACTIONS']."?q=NOT%20merchant_id:".$merchant_id."&fq={!bbox}&sfield=latlon_deal&pt=".$this->user['merchant']['lat'].",".$this->user['merchant']['lon']."&d=".$this->config->config['AROUND_YOU_DISTANCE']."&fq=selfie_time:[NOW-2DAYS%20TO%20NOW]&fl=dist:geodist%28%29,*&wt=json&indent=true&sort=selfie_time%20desc&rows=".$limit."&start=".$start;
                $response = call_api($transactions_url);
            }
            
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
                $response['body'] = $response['body']['response']['docs'];
            }
            else
                $response['body'] = [];
            return $response['body'];
        }
        public function get_around_deals($start,$limit) {
            $merchant_id = $this->user['merchant']['merchantId'];
            if(!empty($this->user['merchant']['lat']) && !empty($this->user['merchant']['lon'])) {
                $transactions_url = $this->config->config['MERCHANT_DEALS']."?q=NOT%20merchant_id:".$merchant_id."*&fq={!bbox}&sfield=latlon&pt=".$this->user['merchant']['lat'].",".$this->user['merchant']['lon']."&d=".$this->config->config['AROUND_YOU_DISTANCE']."&sort=geodist%28%29%20asc&fl=dist:geodist%28%29,*&wt=json&rows=".$limit."&start=".$start;
                $response = call_api($transactions_url);
            }
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
                $response['body'] = $response['body']['response']['docs'];
            }
            else
                $response['body'] = [];

            return $response['body'];
        }

}
