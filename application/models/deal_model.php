<?php
class Deal_model extends CI_Model {

	protected static $token = "LOL";
	protected static $userData;
    public function __construct()
	    {
	    	//$this->load->database();

	    }
	public function saveDeal($userId,$merchantId,$formData,$dealId = false)
		{
			$request = array();
			if($dealId){
				$request['deal']['dealId'] = $dealId;
				$apiUrl = $this->config->config['UPDATE_DEAL'];
			}
			else{
				$apiUrl = $this->config->config['CREATE_DEAL'];
			}
			$request['id'] = $userId;
			$request['token'] = self::$token;
			if(array_key_exists('productId', $formData) && $formData['productId'] != "" && $formData['productId'] != 'new0')
				{
				$request['product']['productId'] = $formData['productId'];
				if($formData['dealType'] == 'allStore')
					{
					$productSku = $merchantId.'_Other';
					$productTitle = 'Other';
					}
				else
					{
					$productSku = $merchantId.'_'.$formData['productSku'];
					$productTitle = $formData['productSku'];
					}
				}
			else
				{
				if($formData['dealType'] == 'allStore')
					{
					$productSku = $merchantId.'_Other';
					$productTitle = 'Other';
					$veg = false;
					$nonVeg = false;
					$mrp = 100.00;
					}
				else
					{
					$productSku = $merchantId.'_'.$formData['productTitle'];
					$productTitle = $formData['productTitle'];
					$veg = array_key_exists('veg',$formData)?filter_var($formData['veg'], FILTER_VALIDATE_BOOLEAN):false;
					$nonVeg = array_key_exists('nonVeg',$formData)?filter_var($formData['nonVeg'], FILTER_VALIDATE_BOOLEAN):false;
					$mrp = $formData['productMrp'];
					}
				$request['product']['productSku'] = $productSku;
				$request['product']['merchantId'] = $merchantId;
				$request['product']['title'] = $productTitle;
				$request['product']['description'] = $productTitle;
				$request['product']['veg'] = $veg;
				$request['product']['nonVeg'] = $nonVeg;
				$request['product']['mrp'] = $mrp;
				}
			$request['deal']['productSku'] = $productSku;
			$request['deal']['merchantId'] = $merchantId;
			$request['deal']['dealPrice'] = array_key_exists('productMrp', $formData) && isset($formData['productMrp'])?$formData['productMrp']:0;
			$request['deal']['dealTitle'] = $formData['dealName'];
			$request['deal']['dealType'] = 'VANILLA';
			$request['deal']['dealTypeId'] = 1;
			$request['deal']['isActive'] = true;
			$request['deal']['termsId'] = 1;
			$request['deal']['dealStartDate'] = date("Y-m-d H:i:s",strtotime($formData['dealStartDatetime']));
			$request['deal']['dealEndDate'] = date("Y-m-d H:i:s",strtotime($formData['dealEndDatetime']));
			$imagesArray = array();
			$formData['deal_images'] = json_decode($formData['deal_images'],true);
			foreach($formData['deal_images'] as $dealImage){
				if(isset($dealImage['url']) && !empty($dealImage['url']))
					{
					$imagesArray[] = array('url'=>$dealImage['url'],'width'=>$dealImage['width'],'height'=>$dealImage['height'],'status'=>null,'imageOrder'=>1,'name'=>'','productSku'=>$productSku);
					}
			}
			//$request['images'] = $imagesArray;
			$formData['rewardType']=='CASHBACK_PERCENT'?$rewardValue=$formData['rewardPercentage']:$rewardValue=$formData['rewardAmount'];
			$rewardsArray = array();
			$rewardsArray[] = array("actionName"=>"BILL","actionThresholdLow"=>intval($formData['minimumBillSize']),"dealType"=>"VANILLA","rewardType"=>$formData['rewardType'],"rewardValue"=>$rewardValue);
			$request['rewards'] = $rewardsArray;
			$requestJson = json_encode($request);
        	$input_headers = array('Content-Type:application/json');
        	//var_dump($requestJson); //exit;
			$response = call_api($apiUrl,$requestJson,$input_headers,$method='post');
			return returnResponseArray($response);
		}
	public function getDeal($userId,$id)
		{
			$apiUrl = $this->config->config['GET_DEAL']."user/".$userId."/id/".$id;
			$response = call_api($apiUrl);
			return returnResponseArray($response);
		}
	public function deleteDeal($id)
		{
			$apiUrl = $this->config->config['DELETE_DEAL']."id/".$id;
			$response = call_api($apiUrl);
			return returnResponseArray($response);
		}
	public function listAllDeals($merchantId)
		{
			$apiUrl = $this->config->config['LIST_DEALS'].$merchantId;
			$response = call_api($apiUrl);
			return returnResponseArray($response);
			/*$list = array(array('id'=>100,'name'=>'abc','stars'=>1),array('id'=>101,'name'=>'xyz','stars'=>2),array('id'=>102,'name'=>'pqr','stars'=>3));*/
		}


	public function listAllProducts($merchantId)
		{
			$apiUrl = $this->config->config['LIST_PRODUCTS'].$merchantId;
			$response = call_api($apiUrl);
			return returnResponseArray($response);
			/*$list = array(array('id'=>100,'name'=>'abc','stars'=>1),array('id'=>101,'name'=>'xyz','stars'=>2),array('id'=>102,'name'=>'pqr','stars'=>3));*/
		}
}