<?php
class Login_model extends CI_Model {
        private $user;
        public function __construct()
        {	
            //$this->load->database();
            $this->load->library('session');
            $this->user = $this->session->all_userdata(); 
        }

        public function first_register($phone) {
        	$response = call_api($this->config->config['FIRST_REGISTER_API'].$phone."/");
        	if(gettype($response['body']) == 'string') {
        		$response['body'] = json_decode($response['body'],true);
        	}
        	return $response['body'];
        }
        public function first_register_validate_otp($phone,$otp) {
        	$response = call_api($this->config->config['FIRST_OTP_VALIDATE_API'].$phone."/otp/".$otp."/");
        	if(gettype($response['body']) == 'string') {
        		$response['body'] = json_decode($response['body'],true);
        	}
        	return $response['body'];
        }
        public function login_user($user_name,$password) {
        	$input = array('username'=>$user_name,'password'=>$password);
        	$input_headers = array('Content-Type:application/json');
        	$response = call_api($this->config->config['LOGIN_USER'],json_encode($input),$input_headers,'post');
        	if(gettype($response['body']) == 'string') {
        		$response['body'] = json_decode($response['body'],true);
        	}
        	return $response['body'];
        }
        public function send_otp($phone) {
            $response = call_api($this->config->config['SEND_OTP_API'].$phone."/");
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function validate_otp($phone,$otp) {
            $response = call_api($this->config->config['VALIDATE_OTP_API'].$phone."/otp/".$otp."/");
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function update_password($phone,$password) {
            $input = array('username'=>$phone,'password'=>$password);
            $input_headers = array('Content-Type:application/json');
            $response = call_api($this->config->config['UPDATE_USER_PASSWORD'],json_encode($input),$input_headers,'post');
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function check_user_exists($phone) {
            $response = call_api($this->config->config['CHECK_USER_EXIST'].$phone."/");
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            if($response['body']['status'] == 'SUCCESS')
                return true;
            return false;
        }
        public function update_user_name($user_name) {
            
            $response = call_api($this->config->config['UPDATE_USER_NAME'].$this->user['phone_no']."/username/".$user_name."/");
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function edit_merchant($merchant,$merchant_images) {
            global $login_statuses;
            $input_headers = array('Content-Type:application/json');
            $input = array('id'=>$this->user['user_id'],'merchant'=>$merchant,'images'=>$merchant_images);
            if($this->user['status'] == $login_statuses['edit_profile'])
                $response = call_api($this->config->config['CREATE_MERCHANT'],json_encode($input),$input_headers,'post');
            else
                $response = call_api($this->config->config['UPDATE_MERCHANT'],json_encode($input),$input_headers,'post');
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function edit_password($phone,$password) {
            $input_headers = array('Content-Type:application/json');
            $input = array('username'=>$phone,'password'=>$password);
            $response = call_api($this->config->config['UPDATE_USER_PASSWORD'],json_encode($input),$input_headers,'post');
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function dashboard_summary($x) {
            $end_date = date('Y-m-d');
            $start_date = date('Y-m-d',strtotime('-'.$x.' day'));
            $url = $this->config->config['MERCHNAT_DASHBOARD_SUMMARY'].$this->user['merchant']['merchantId']."/startDate/".$start_date."/endDate/".$end_date;
            $response = call_api($url);
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function get_merchant_data($user_id) {
            $response = call_api($this->config->config['MERCHANT_DETAIL'].$user_id);
            if(gettype($response['body']) == 'string') {
                $response['body'] = json_decode($response['body'],true);
            }
            return $response['body'];
        }
        public function get_other_deal_id($merchantId) {
            $apiUrl = $this->config->config['LIST_DEALS'].$merchantId;
            $response = call_api($apiUrl);
            $response = returnResponseArray($response);
            foreach ($response['results'] as  $value) {
                if(strpos('Other#',trim($value['productSku'])) == 0) {
                    return $value['dealId'];
                }
            }
            return false;   
        }
}