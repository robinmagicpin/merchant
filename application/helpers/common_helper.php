<?php
function call_api($url,$input=[],$input_headers=[],$method='get') {
	$is_post = false;
	if(strtolower($method)=='post')
		$is_post = true;
	$ch = curl_init();
	$url = str_replace(' ', '+', $url);
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_POST,$is_post);
	if($is_post) {
		curl_setopt($ch, CURLOPT_HTTPHEADER,$input_headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$input);
	}
	//curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,5); 
	//curl_setopt($ch, CURLOPT_TIMEOUT, 500);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HEADER,$is_post);
	$response = curl_exec($ch);
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);
	curl_close($ch);
	if(!$is_post)
		$body = $response;
	return array('body'=>$body);
}

function get_random_string($length,$valid_chars="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
{
    $random_string = "";
    $num_valid_chars = strlen($valid_chars);
    for ($i = 0; $i < $length; $i++)
    {
        $random_pick = mt_rand(1, $num_valid_chars);
        $random_char = $valid_chars[$random_pick-1];
        $random_string .= $random_char;
    }
    return $random_string;
}

function validate_phone_no($phone_no) {
	$status = 'SUCCESS';
	$length = strlen($phone_no);
	if($length ==10)
		$phone_no = "91".$phone_no;
	elseif ($length == 11) {
		$phone_no = "91".ltrim($phone_no,"0");
	}
	else if($length !=12)
		$status = 'FAILURE';
	return array('status'=>$status,'phone_no'=>$phone_no);

}
function raw_localities() {
	$url="http://search.api.magicpin.in:8983/solr/geo/select?q=*:*&fl=*&wt=json&indent=true&rows=500";
	$response = call_api($url);
	$localities = json_decode($response['body'],true);
	$localities = $localities['response']['docs'];
	$to_return = [];
	$all_localities = [];
	$all_states = [];
	$all_cities = [];
	foreach ($localities as $key => $value) {
		$to_return['all_localities'][] = trim($value['locality'])."--".trim($value['city']);
		$to_return['all_states'][] = trim($value['state']);
		$to_return['all_cities'][] = trim($value['city'])."--".trim($value['state']);
	}
	$to_return['all_localities']=array_unique($to_return['all_localities']);
	$to_return['all_states']=array_unique($to_return['all_states']);
	$to_return['all_cities']=array_unique($to_return['all_cities']);
	return $to_return;
}
function returnResponseArray($response) {
	if(array_key_exists('body', $response))
		$response = json_decode($response['body'],true);
	else
		$response = json_decode($response,true);
	return $response;
}

function get_period_dates($period,$begin_date='') {

	$str = "1DAY";
	switch(strtolower($period)) {
		case 'today':
			$str = "1DAY";
			break;
		case 'week':
			$str = "7DAY";
			break;
		case 'all':
			$str = "10YEAR";
			break;
	}
	return $str;
}
function generateEditDealFormData($response,$userId,$merchantId,$token) {
	$formData = array();	
	$formData['id'] = $userId;
	$formData['token'] = $token;
	$formData['productId'] = $response['product']['productId'];

	$formData['productSku'] = $response['product']['title'];
	$formData['productTitle'] = $response['product']['title'];
	$productSkuArray = explode('_',$response['product']['productSku']);
	if($productSkuArray[1] == 'Other')
		$formData['dealType'] = 'allStore';
	else
		$formData['dealType'] = 'sku';
	$formData['veg'] = $response['product']['veg'];
	$formData['nonVeg'] = $response['product']['nonVeg'];
	$formData['productMrp'] = $response['product']['mrp'];
	$formData['dealId'] = $response['deal']['dealId'];
	$formData['dealName'] = $response['deal']['dealTitle'];
	//$formData['dealType'] = 'VANILLA';
	$formData['dealTypeId'] = 1;
	$formData['isActive'] = true;
	$formData['termsId'] = 1;
	$formData['dealStartDatetime'] = date("d-m-Y H:i:s",strtotime($response['deal']['dealStartDate']));
	$formData['dealEndDatetime'] = date("d-m-Y H:i:s",strtotime($response['deal']['dealEndDate']));
	$imagesArray = array();
	$dealImagesArray = $response['images'];
	foreach($dealImagesArray as $dealImage){
		$imagesArray[] = array('imageUrl'=>$dealImage['url'],'imageWidth'=>$dealImage['width'],'imageHeight'=>$dealImage['height'],'status'=>null,'imageOrder'=>1,'imageName'=>$dealImage['name']);
	}
	$formData['dealImages'] = $imagesArray;
	foreach($response['rewards'] as $rewardsArray)
		{	
		$formData['actionName'] = $rewardsArray['actionName'];
		$formData['minimumBillSize'] = $rewardsArray['actionThresholdLow'];
		//$formData['dealType'] = $rewardsArray['dealType'];
		$formData['rewardType'] = $rewardsArray['rewardType'];
		$formData['rewardType']=='CASHBACK_PERCENT'?$formData['rewardPercentage']=$rewardsArray['rewardValue']:$formData['rewardAmount']=$rewardsArray['rewardValue'];
		}
	return $formData;
	}
