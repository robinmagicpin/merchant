<script type="text/javascript" src="<?php echo base_url(); ?>js/deal.js"></script>
</head>
<body>
    <header class="header" id ="header">
	<!--header top section ends here -->	
		<div class="header-top">
			<div class="inner-wrapper container">
				<div class="row">
					<div class="col-md-12">
						<div class="logo">
							<img src="<?php echo base_url(); ?>images/logo.png" alt="magicpin" />
						</div>
						<div class="pull-right profile-logout">
							<ul>
								<li><a href="<?php echo base_url; ?>login/edit_profile/">My Account</a></li>
								<li><a href="<?php echo base_url; ?>login/logout/">Log Out</a></li>
							</ul>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="header-bottom">
			<nav class="navbar navbar-default navbar-static-top">
			  <div class="container">
			  <div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
				  <ul class="nav navbar-nav">
					<li ><a href="<?php echo base_url; ?>login/dashboard/">Home</a></li>
					<li><a href="<?php echo base_url; ?>deal/">My Deals</a></li>
					<li><a href="<?php echo base_url; ?>customer/">My Customers</a></li>
					<li><a href="<?php echo base_url; ?>store/">My Store</a></li>
					<li><a href="<?php echo base_url; ?>store/around_you/">Around Me</a></li>
				  </ul>
				  
				</div><!--/.nav-collapse -->
			  </div>
			</nav>
			
			</div>
	</header>
<?php echo $this->session->flashdata('flash_message'); ?>
<section class="main-container" id ="register-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="register-wrapper">
					
					<div class="title-bar"><h2><span class="icon"><i class="fa fa-pencil-square-o"></i>
</span>Edit deal</h2></div>
					<section class="profile-detail-section">
					<div class="row">
						<div class="col-md-12 imageFormContainer">
							<form method="post" class="imageForm">
							<div class="profile-thumb">
								<input type="file" class="filestyle" name="dealImage" accept="image/png,image/jpeg,.webp" data-buttonBefore="true">
							</div>
							<input class="btn btn-default imageUploadButton image_upload_button" placeholder="" type="submit">
							</form>
							<div class="profile-tag">
							Upload deal pictures here
							</div>
						</div>
					</div>
					</section>
					<!-- give deal section -->
				<?php echo form_open_multipart(base_url().'deal/post/'.$formData['dealId']); ?>
				<section class="give-deal">
					<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="hidden"  id="dealImageActual" name="dealImageActual" value='<?php echo isset($formData["dealImages"])?json_encode($formData["dealImages"]):"{}"; ?>'>
								<input type="text" id="dealName" name="dealName" class="form-control input-medium" value="<?php echo !isset($formData['dealName'])?'':$formData['dealName']; ?>"  placeholder="Deal Name"/>
							</div>
						</div>
					</div>
					</div>
					<div class="title-bar"><h3>Give Deal On</h3></div>
					<div class="row">
						<div class="col-md-4 col-xs-12">
							
							<div class="radio radio-info">
								<input type="radio" id="dealTypeAllStore" name="dealType" value="allStore" <?php echo !isset($formData['dealType']) || $formData['dealType'] != 'sku' ? "checked" : ""; ?> >
								<label for="dealTypeAllStore"><span class="icon">
</span> All Store </label>
							</div>
							<div class="radio radio-info">
								<input type="radio" id="dealTypeSku" name="dealType" value="sku" <?php echo isset($formData['dealType']) && $formData['dealType'] != 'allStore' ? "checked" : ''; ?> disabled >
								<label for="dealTypeSku"> <span class="icon"><i class="fa fa-inr"></i>
</span>SKU </label>
							</div>
						</div>
						<div class="col-md-8 col-xs-12" <?php echo isset($formData['dealType']) && $formData['dealType'] != 'sku' ? 'style="display:none"' : ''; ?> id="dealTypeHidden">
							<div class="spinner-block">
								<div class="input-group sku-detail">
									<div>
										<p>Enter SKU Details</p>
										<div class="form-group">
											<input type="text" id="productSku" name="productSku" class="form-control" value="<?php echo !isset($formData['productSku'])?'':$formData['productSku']; ?>"  placeholder="Name Of SKU" readonly="readonly" />
										</div>
										<input type="hidden" id="productId" name="productId" class="form-control" value="<?php echo !isset($formData['productId'])?'':$formData['productId']; ?>" />
										<input type="hidden" id="dealPrice" name="dealPrice" class="form-control" value="<?php echo !isset($formData['dealPrice'])?'':$formData['dealPrice']; ?>" placeholder="Deal Price"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					</section>
					
					<!-- set reward section -->
					<section class="set-reward">
					<div class="title-bar"><h3>Set Reward</h3></div>
					<div class="row">
						<div class="col-md-4 col-xs-12">
							
							<div class="radio radio-info">
								<input type="radio" id="rewardTypePercentage" name="rewardType" value="CASHBACK_PERCENT" <?php echo !isset($formData['rewardType']) || $formData['rewardType'] == 'CASHBACK_PERCENT' ? 'checked' : ''; ?> >
								<label for="rewardTypePercentage"><span class="icon">%
</span> Discount </label>
							</div>
							<div class="radio radio-info">
								<input type="radio" id="rewardTypeAmount" name="rewardType" value="CASHBACK_FLAT" <?php echo isset($formData['rewardType']) && $formData['rewardType'] == 'CASHBACK_FLAT' ? 'checked' : ''; ?> >
								<label for="rewardTypeAmount"> <span class="icon"><i class="fa fa-inr"></i>
</span>Cashback </label>
							</div>
						</div>
						<div class="col-md-8 col-xs-12">
						<p>Use the +/- to change value</p>
						<div class="spinner-block">
								<div <?php echo !isset($formData['rewardType']) || $formData['rewardType'] != 'CASHBACK_FLAT' ? 'style="display:none"' : ''; ?> id="rewardAmountHidden" class="input-group spinner">
									<input type="text" id="rewardAmount" name="rewardAmount" class="form-control" value="<?php echo !isset($formData['rewardAmount'])?'0':$formData['rewardAmount']; ?>" />
									<div class="input-group-btn-vertical">
									  <button class="btn btn-incre btn-spinner" type="button"><i class="fa fa-plus"></i></button>
									  <button class="btn btn-decre btn-spinner" type="button"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div <?php echo isset($formData['rewardType']) && $formData['rewardType'] != 'CASHBACK_PERCENT' ? 'style="display:none"' : ''; ?> id="rewardPercentageHidden" class="input-group spinner">
									<input type="text" id="rewardPercentage" name="rewardPercentage" class="form-control" value="<?php echo !isset($formData['rewardPercentage'])?'0':$formData['rewardPercentage']; ?>" />
									<div class="input-group-btn-vertical">
									  <button class="btn btn-incre btn-spinner" type="button"><i class="fa fa-plus"></i></button>
									  <button class="btn btn-decre btn-spinner" type="button"><i class="fa fa-minus"></i></button>
									</div>
								</div>
						  	</div>
						</div>
					</div>
					</section>
					
					<!-- set minimum bill section -->
					<section class="min-bill-section">
					<div class="title-bar"><h3>SET MINIMUM BILL SIZE</h3></div>
					<div class="row">
						
						<div class="col-md-12 col-xs-12">
						<p>Use the +/- to change value</p>
						<div class="spinner-block">
						<div class="input-group spinner">
							<input type="number" id="minimumBillSize" name="minimumBillSize" value="<?php echo !isset($formData['minimumBillSize'])?'0':$formData['minimumBillSize']; ?>" class="form-control">
							<div class="input-group-btn-vertical">
							  <button class="btn btn-incre btn-spinner" type="button"><i class="fa fa-plus"></i></button>
							  <button class="btn btn-decre btn-spinner" type="button"><i class="fa fa-minus"></i></button>
							</div>
						  </div>
						  </div>
						</div>
					</div>
					</section>
					
					<!-- set deal duration section -->
					<section class="deal-duration">
					<div class="title-bar"><h3>Set Deal Duration</h3></div>
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="calender-startDate">
							   <div class="form-group" id="calender-input">
							   <span class="icon"><i class="fa fa-calendar"></i></span>
								<input type="text"  data-field="datetime" data-format="dd-MM-yyyy hh:mm:ss" data-view="Popup" data-startend="start" data-startendelem="#dealEndDatetime" id="dealStartDatetime" name="dealStartDatetime" value="<?php echo !isset($formData['dealStartDatetime'])?'':$formData['dealStartDatetime']; ?>" placeholder="Select Start Date & Time" readonly />
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="calender-endDate">
							   <div class="form-group" id="calender-input">
							   <span class="icon"><i class="fa fa-calendar"></i></span>
								<input type="text"  data-field="datetime" data-format="dd-MM-yyyy hh:mm:ss" data-view="Popup" data-startend="end" data-startendelem="#dealStartDatetime" id="dealEndDatetime" name="dealEndDatetime" value="<?php echo !isset($formData['dealEndDatetime'])?'':$formData['dealEndDatetime']; ?>" placeholder="Select End Date & Time" readonly />
								</div>
							</div>
						</div>
						<div id="dtBox"></div>
					</div>
					</section>	
					<div class="btn-group-top btn-group" >
						<input type="submit" class="btn btn-default">
					</div>
					<div class="btn-group-top btn-group" >
						<a href="<?php echo base_url().'deal'; ?>" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>

<?php echo form_close();?>
<script>
    var productAutofillData = <?php echo $productAutofillData; ?>
  
    $( "#productSku" ).autocomplete({
      source:productAutofillData,
      minLength: 1,
      response: function( event, ui ) {
      	ui.content.push({'label':'Add New', 'value': 'new0'});
      },
      select: function (event, ui) {
        				event.preventDefault();
                         $("#productSku").val(ui.item.label);
                         $("#productSku").attr("value",ui.item.label);
                         $("#productId").val(ui.item.value);
						 $("#productSku").change();

                     },

    });
</script>
<script type="text/javascript">
				$("#dtBox").DateTimePicker({
				
					dateFormat: "MM-dd-yyyy",
					timeFormat: "HH:mm",
					dateTimeFormat: "dd-MM-yyyy hh:mm:ss"
				
				});
		
</script>

<script>
	$(":file").filestyle({buttonBefore: true});
</script>
