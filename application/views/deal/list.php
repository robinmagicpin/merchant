<script type="text/javascript">
      var all_sku_deal = '<?php echo isset($user["all_store_deal"])?$user["all_store_deal"]:""; ?>';
  </script>
</head>
<body>
  <script type="text/javascript" src="<?php echo base_url; ?>js/deal_listing.js?v=<?php echo JS_VERSION; ?>">
  </script>
 
        <section class="create-section-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="create-add-btn">
                        <a href="#" class="btn btn-primary">
                        Create Deal<span class="icon"><i class="fa fa-plus"></i></span>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Supporting Link section-->
        <section class="support-links">
        <div class="container">
        	
            <div class="row">
                <ul class="breadcrumbs col-md-5">
                    <li class="active today_time" id="today_time_button"><span class="icon"><i class="fa fa-tags"></i></span><a href="#">Running</a>
                    </li>
                    <li class="week_time" id="week_time_button"id="week_time_button"><a href="#">Ended</a></li>
                    <li class="all_time" id="all_time_button"><a href="#">All</a></li>
                </ul>
            </div>
        </div>
    </section>
	<section class="carousel-wrapper">
		<section class="customer-carousel-section container">
			<div class="row">
			<div class="col-md-12">
			
        <div class="row deal_listing_today" id="deal_listing_today">
          <div id="deal_listing_today_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
			  </div>
        <div class="row deal_listing_week" id="deal_listing_week" style="display:none;">
          <div id="deal_listing_week_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
        </div>
        <div class="row deal_listing_all" id="deal_listing_all" style="display:none;">
          <div id="deal_listing_all_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
        </div>
			</div>
      </div>
		</section>
	</section>
	
	
	
	<script>
	$(":file").filestyle({buttonBefore: true});
	(function ($) {
  $('.spinner #btn1').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  });
  $('.spinner #btn2').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
  });
})(jQuery);
//for carousel
$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
	</script>

</body>
