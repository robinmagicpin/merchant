<script type="text/javascript" src="<?php echo base_url; ?>js/deal.js?v=<?php echo JS_VERSION; ?>"> </script>
</head>
<body>
	<?php 
		$deal_images = isset($deal_data["dealImages"]) && sizeof($deal_data["dealImages"])>0?str_replace("'","",json_encode($deal_data["dealImages"])):"{}";
		$editing = (isset($deal_data['dealId']) && $deal_data['dealId'] >0)?true:false;
		$deal_id = 	(isset($deal_data['dealId']) && $deal_data['dealId'] >0)?$deal_data['dealId']:0;
	?>
	<script type="text/javascript">
		var all_deal_images = JSON.parse('<?php echo $deal_images;?>');
		var editing = '<?php echo $editing; ?>';
		var deal_id = '<?php echo $deal_id; ?>';
	</script>
	

    
<?php //echo $this->session->flashdata('flash_message'); ?>
<div id="deal_alert_messages">
	<div class="alert alert-success deal_update_success" role="alert" style="display:none;text-align:center;"></div>
	<div class="alert alert-danger deal_update_failure" role="alert" style="text-align:center;display:none;"></div>
</div>
<section class="main-container" id ="register-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="register-wrapper">
					
					<div class="title-bar"><h2>
						<span class="icon"><i class="fa fa-pencil-square-o"></i>
						<?php if(isset($deal_data['dealId']) && $deal_data['dealId']>0): ?>
							</span>Edit deal</h2>
						<?php else: ?>
							</span>Create deal</h2>
						<?php endif; ?>
					</div>
					
					<section class="profile-detail-section">
					<div class="row">
						<div class="col-md-12 imageFormContainer">
							<div class="profile-thumb">
								<form method="post" class="imageForm">
									<input type="file" class="filestyle" name="dealImage" accept="image/png,image/jpeg,.webp" data-buttonBefore="true">
									<input class="btn btn-default image_upload_button" id="image_submit" type="submit">
									<div class="edit_profile_image_upload_loader image_upload_button"><i class="fa fa-cog fa-spin"></i></div>
							</form>
							</div>
							<div class="profile-tag">
							Upload deal pictures here
							</div>
						</div>
					</div>
					</section>
					<!-- give deal section -->
				<form method="post" class="deal_form">
				<section class="give-deal">
					<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" id="dealName" name="dealName" class="form-control input-medium" value="<?php echo !isset($deal_data['dealName'])?'':$deal_data['dealName']; ?>"  placeholder="Deal Name" required />
							</div>
						</div>
					</div>
					</div>
					<div class="title-bar"><h3>Give Deal On</h3></div>
					<div class="row">
						<div class="col-md-4 col-xs-12">
							
							<div class="radio radio-info">
								<input type="radio" id="dealTypeAllStore" name="dealType" value="allStore" <?php echo !isset($deal_data['dealType']) || $deal_data['dealType'] != 'sku' ? "checked" : "checked"; ?> >
								<label for="dealTypeAllStore"><span class="icon">
								</span> All Store </label>
							</div>
							<input type="hidden" name="productId" value="<?php echo !isset($deal_data['productId'])?'':$deal_data['productId']; ?>" />
							<div class="radio radio-info">
								<input type="radio" id="dealTypeSku" name="dealType" value="sku" <?php echo isset($formData['dealType']) && $formData['dealType'] != 'allStore' ? "checked" : ''; ?> disabled >
							<!--<div class="radio radio-info">
								<input type="radio" id="dealTypeSku" name="dealType" value="sku" <?php //echo isset($deal_data['dealType']) && $deal_data['dealType'] != 'allStore' ? "checked" : ''; ?> >
								<label for="dealTypeSku"> <span class="icon"><i class="fa fa-inr"></i>
</span>SKU </label>
							</div>
						</div>
						<div class="col-md-8 col-xs-12" <?php //echo !isset($formData['dealType']) || $formData['dealType'] != 'sku' ? 'style="display:none"' : ''; ?> id="dealTypeHidden">
							<div class="spinner-block">
								<div class="input-group sku-detail">
									<div>
										<p>Enter SKU Details</p>
										<div class="form-group">
											<input type="text" id="productSku" name="productSku" class="form-control" value="<?php echo !isset($formData['productSku'])?'':$formData['productSku']; ?>"  placeholder="Name Of SKU"/>
										</div>
										<input type="hidden" id="productId" name="productId" class="form-control" value="<?php echo !isset($formData['productId'])?'':$formData['productId']; ?>" />
										<input type="hidden" id="dealPrice" name="dealPrice" class="form-control" value="<?php echo !isset($formData['dealPrice'])?'':$formData['dealPrice']; ?>" placeholder="Deal Price"/>
									</div>
								</div>
							</div>
						</div>-->
					</div>
					</section>
					
					<!-- set reward section -->
					<section class="set-reward">
					<div class="title-bar"><h3>Set Reward</h3></div>
					<div class="row">
						<div class="col-md-4 col-xs-12">
							
							<div class="radio radio-info ">
								<input type="radio" id="rewardTypePercentage" name="rewardType" value="CASHBACK_PERCENT" <?php echo !isset($deal_data['rewardType']) || $deal_data['rewardType'] == 'CASHBACK_PERCENT' ? 'checked' : ''; ?> >
								<label for="rewardTypePercentage"><span class="icon">%
</span> Discount </label>
							</div>
							<div class="radio radio-info ">
								<input type="radio" id="rewardTypeAmount" name="rewardType" value="CASHBACK_FLAT" <?php echo isset($deal_data['rewardType']) && $deal_data['rewardType'] == 'CASHBACK_FLAT' ? 'checked' : ''; ?> >
								<label for="rewardTypeAmount"> <span class="icon"><i class="fa fa-inr"></i>
</span>Cashback </label>
							</div>
						</div>
						<div class="col-md-8 col-xs-12">
						<p>Use the +/- to change value</p>
						<div class="spinner-block">
								<div <?php echo !isset($formData['rewardType']) || $formData['rewardType'] != 'CASHBACK_FLAT' ? 'style="display:none"' : ''; ?> id="rewardAmountHidden" class="input-group spinner">
									<input type="text" id="rewardAmount" name="rewardAmount" class="form-control" value="<?php echo !isset($deal_data['rewardAmount'])?'0':$deal_data['rewardAmount']; ?>" />
									<div class="input-group-btn-vertical">
									  <button class="btn btn-incre btn-spinner" type="button" onclick="bill_amount_increment();"><i class="fa fa-plus"></i></button>
									  <button class="btn btn-decre btn-spinner" type="button" onclick="bill_amount_decrement();"><i class="fa fa-minus"></i></button>
									</div>
								</div>
								<div <?php echo isset($formData['rewardType']) && $formData['rewardType'] != 'CASHBACK_PERCENT' ? 'style="display:none"' : ''; ?> id="rewardPercentageHidden" class="input-group spinner">
									<input type="text" id="rewardPercentage" name="rewardPercentage" class="form-control" value="<?php echo !isset($deal_data['rewardPercentage'])?25:$deal_data['rewardPercentage']; ?>%" />
									<div class="input-group-btn-vertical">
									  <button class="btn btn-incre btn-spinner" type="button" onclick="bill_discount_increment();"><i class="fa fa-plus"></i></button>
									  <button class="btn btn-decre btn-spinner" type="button" onclick="bill_discount_decrement();"><i class="fa fa-minus"></i></button>
									</div>
								</div>
						  	</div>
						</div>
					</div>
					</section>
					
					<!-- set minimum bill section -->
					<section class="min-bill-section">
					<div class="title-bar"><h3>SET MINIMUM BILL SIZE</h3></div>
					<div class="row">
						
						<div class="col-md-4 col-xs-12">
							<p>Use the +/- to change value</p>
						</div>
						<div class="col-md-6 col-xs-12">
						<div class="spinner-block">
						<div class="input-group spinner">
							<input type="text" id="minimumBillSize" name="minimumBillSize" value="₹<?php echo !isset($deal_data['minimumBillSize'])?'100':$deal_data['minimumBillSize']; ?>" class="form-control">
							<div class="input-group-btn-vertical">
							  <button class="btn btn-incre btn-spinner" type="button" onclick="bill_size_increment();"><i class="fa fa-plus"></i></button>
							  <button class="btn btn-decre btn-spinner" type="button" onclick="bill_size_decrement();"><i class="fa fa-minus"></i></button>
							</div>
						  </div>
						  </div>
						</div>
					</div>
					</section>
					
					<!-- set deal duration section -->
					<section class="deal-duration">
					<div class="title-bar"><h3>Set Deal Duration</h3></div>
					<div class="row">
						<div class="col-md-6 col-xs-12">
							<div class="calender-startDate">
							   <div class="form-group" id="calender-input">
							   <span class="icon"><i class="fa fa-calendar"></i></span>
								<input type="text"  data-field="datetime" data-format="dd-MM-yyyy hh:mm" data-view="Popup" data-startend="start" data-startendelem="#dealEndDatetime" id="dealStartDatetime" name="dealStartDatetime" value="<?php echo !isset($deal_data['dealStartDatetime'])?'':$deal_data['dealStartDatetime']; ?>" placeholder="Select Start Date & Time" readonly />
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="calender-endDate">
							   <div class="form-group" id="calender-input">
							   <span class="icon"><i class="fa fa-calendar"></i></span>
								<input type="text"  data-field="datetime" data-format="dd-MM-yyyy hh:mm" data-view="Popup" data-startend="end" data-startendelem="#dealStartDatetime" id="dealEndDatetime" name="dealEndDatetime" value="<?php echo !isset($deal_data['dealEndDatetime'])?'':$deal_data['dealEndDatetime']; ?>" placeholder="Select End Date & Time" readonly />
								</div>
							</div>
						</div>
						<div id="dtBox"></div>
					</div>
					</section>
					<div id="deal_save_buttons">
						<div class="btn-group-top btn-group" >
							<input type="submit" class="btn btn-default" id="save_deal">
						</div>
						<div class="btn-group-top btn-group">
							<a onclick="window.history.back();" class="btn btn-default">Cancel</a>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
