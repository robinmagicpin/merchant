<script type="text/javascript">
        var all_sku_deal = '<?php echo isset($user["all_store_deal"])?$user["all_store_deal"]:""; ?>';
</script>
</head>
<body>
<script type="text/javascript" src="<?php echo base_url; ?>js/customer_listing.js?v=<?php echo JS_VERSION; ?>"></script>
   
	<section class="create-section-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="create-add-btn">
				<a href="#" class="btn btn-primary">
				Create Deal <span class="icon"><i class="fa fa-plus"></i></span>
				</a>
			</div>
			</div>
		</div>
	</div>
	</section>

    <section class="support-links">
        <div class="container">
            <div class="row">
                <ul class="breadcrumbs col-md-5">
                    <li class="active today_time" id="today_time_button"><span class="icon"><i class="fa fa-tags"></i></span><a href="#">Today</a>
                    </li>
                    <li class="week_time" id="week_time_button"><a href="#" >This Week</a></li>
                    <li class="all_time" id="all_time_button"><a href="#" >All</a></li>
                </ul>
                <div class="calender-wrapper col-md-6 col-md-offset-1">
					<div class="calender-startDate">
                    <input type="text"  data-field="date" data-format="yyyy-MM-dd" data-view="Popup" data-startend="start" data-startendelem="#dealEndDatetime" id="start_date" placeholder="start Date" readonly />
					<span class="icon"><i class="fa fa-calendar"></i></span>
					</div>
					<div class="calender-endDate">
					<span class="icon"><i class="fa fa-calendar"></i></span>
                    <input type="text"  data-field="date" data-format="yyyy-MM-dd" data-view="Popup" data-startend="end" data-startendelem="#dealStartDatetime" id="end_date" placeholder="end Date" readonly />
					</div>
                    <div id="dtBox"></div>
                </div>
            </div>
        </div>
    </section>

	<section class="customer-selfie-container">
    <section class="customer-carousel-section container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-bar">
                    <h3><span class="icon"><i class="fa fa-user"></i></span>Customers</h3>
                </div>

			<div class="row customer_listing_today" id="customer_listing_today">
                <div id="customer_listing_today_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
			</div>
            <div class="row customer_listing_week" id="customer_listing_week" style="display:none;">
                <div id="customer_listing_week_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
            </div>
            <div class="row customer_listing_all" id="customer_listing_all" style="display:none;">
                <div id="customer_listing_all_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
            </div>
            <div class="row customer_listing_generic" id="customer_listing_generic" style="display:none;">
                <div id="customer_listing_generic_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
            </div>

            </div>
        </div>
    </section>
	</section>
    <script>
$(":file").filestyle({buttonBefore: true});
    (function ($) {
    $('.spinner #btn1').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
    });
    $('.spinner #btn2').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
    });
    })(jQuery);
    //for carousel
    function move_customer_carousel() {
        $('.carousel[data-type="multi"] .item').each(function(){
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i=0;i<2;i++) {
            next=next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

        next.children(':first-child').clone().appendTo($(this));
        }
        });
    }
    </script>
</body>

