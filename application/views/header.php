<!DOCTYPE html>
<html lang="en">
<head>
<title>Magicpin</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="<?php echo base_url; ?>css/library/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url; ?>css/library/chosen.css">
<link rel="stylesheet" href="<?php echo base_url; ?>css/library/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url; ?>css/library/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url; ?>css/library/build.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url; ?>css/styles.css?v=<?php echo CSS_VERSION; ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url; ?>css/library/DateTimePicker.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url; ?>css/library/fancybox.css">

<script type="text/javascript" src="<?php echo base_url; ?>js/library/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/chosen.jquery.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/jquery.MetaData.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/jquery.MultiFile.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/DateTimePicker.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url; ?>js/library/fancybox.js"></script>

<script> var base_url = "<?php echo base_url; ?>"</script>
<script type="text/javascript" src="<?php echo base_url; ?>js/global.js?v=<?php echo JS_VERSION; ?>"></script>
<?php global $login_statuses;
 ?>
<?php if(isset($user) && !empty($user['status'])): ?>
<header class="header" id ="header">
	<div class="header-top">
		<div class="inner-wrapper container">
			<div class="row">
				<div class="col-md-12">
					<a href="<?php echo base_url; ?>login/dashboard/">
						<div class="logo">
							<img src="<?php echo base_url; ?>images/logo.png" alt="magicpin" />
						</div>
					</a>
					<div class="pull-right profile-logout" role = "navigation">
                               <ul>
                                  <li class="dropdown">
                                  <a href="#" class ="dropdown-toggle" data-toggle = "dropdown">
                                      My Account 
                                      <b class="caret"></b>
                                  </a>
                                                  <ul class="dropdown-menu">
                                               <li><a href="#">Edit Profile</a></li>
                                           <li><a href="#">Help</a></li>
                                           <li><a href="#">Logout</a></li>
                                       </ul>
                                  </li>
                                  <li><img src="images/Addiction.jpg" height="40" class="Merchant-Small_logo" /></li>
                               </ul>
                           </div>
					<!--<div class="pull-right profile-logout">
						<ul>
						<?php //if($user['status'] == $login_statuses['dashboard']): ?>
							<?php //if($current_method != 'edit_profile'): ?>
								<li><a href="<?php echo base_url; ?>login/edit_profile/">My Account</a></li>
							<?php //else: ?>
								<li><a href="<?php echo base_url; ?>login/dashboard">Dashboard</a></li>
							<?php// endif ?>	
						<?php// endif ?>
							<li><a href="<?php echo base_url; ?>login/logout/">Log Out</a></li>
						</ul>
                	</div>->>
				</div>
			</div>
		</div>
	</div>
	<?php if($user['status'] == $login_statuses['dashboard']): ?>
	<div class="header-bottom">
		<nav class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
				    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
				  	</button>
				</div>
				<div id="navbar" class="navbar-collapse collapse">
				  	<ul class="nav navbar-nav">
				  		<?php if(isset($dashboard_active)): ?>
							<li class="active"><a href="<?php echo base_url; ?>login/dashboard/">Home</a></li>
						<?php else: ?>
							<li ><a href="<?php echo base_url; ?>login/dashboard/">Home</a></li>
						<?php endif ?>
						<?php if(isset($deal_list_active)): ?>
							<li class="active"><a href="<?php echo base_url; ?>deal/">My Deals</a></li>
						<?php else: ?>
							<li><a href="<?php echo base_url; ?>deal/">My Deals</a></li>
						<?php endif ?>
						<?php if(isset($customer_list_active)): ?>
							<li class="active"><a href="<?php echo base_url; ?>customer/">My Customers</a></li>
						<?php else: ?>
							<li><a href="<?php echo base_url; ?>customer/">My Customers</a></li>
						<?php endif ?>
						<?php if(isset($store_active)): ?>
							<li class="active"><a href="<?php echo base_url; ?>store/">My store</a></li>
						<?php else: ?>
							<li><a href="<?php echo base_url; ?>store/">My Store</a></li>
						<?php endif ?>
						<?php if(isset($around_you_list_active)): ?>
							<li class="active"><a href="<?php echo base_url; ?>store/around_you/">Around Me</a></li>
						<?php else: ?>
							<li><a href="<?php echo base_url; ?>store/around_you/">Around Me</a></li>
						<?php endif ?>
				  	</ul>
				</div><!--/.nav-collapse -->
			</div>
		</nav>
	</div>
	<?php endif ?>
</header>
<?php endif ?>

