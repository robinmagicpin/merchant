<script type="text/javascript">
  var all_sku_deal = '<?php echo isset($user["all_store_deal"])?$user["all_store_deal"]:""; ?>';
</script>
</head>
    <body>
    <script type="text/javascript" src="<?php echo base_url; ?>js/store.js?v=<?php echo JS_VERSION; ?>">
    </script>
    
        <section class="create-section-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="create-add-btn">
                        <a href="#" class="btn btn-primary">
                        Create Deal<span class="icon"><i class="fa fa-plus"></i></span>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </section>
        <!--view store header block section-->
        <section class="view-store-header-block">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                	<div class="row">
                    	<div class="col-md-4">

                    	<?php if(sizeof($user['store_images']) >0): ?>

                        	<img width="100px;" height="100px;" src="<?php echo $user['store_images'][0]['url']; ?>" alt="" />
                        <?php endif ?>
                        </div>
                    	<div class="col-md-8 view-store-random-txt">
                        	<div class="header-txt">
                            	<?php echo $user['merchant']['merchantName']; ?><br/>
                              <?php $addressLine =$user['merchant']['addressLine1'].",".$user['merchant']['city'].",".$user['merchant']['state'];
                                $addressLine = str_replace("_", " " ,$addressLine);
                                $addressLine = str_replace(";", " " ,$addressLine);
                                $addressLine_arr = explode(",",$addressLine);
                                $addressLine_arr = array_unique($addressLine_arr);
                                $addressLine= implode(",",$addressLine_arr);
                                $addressLine_arr = explode(" ",$addressLine);
                                $addressLine_arr = array_unique($addressLine_arr);
                                $addressLine = implode(" ",$addressLine_arr);
                              ?>
                                <span><?php echo $addressLine; ?></span>
                            </div>
                            <ul>
                            	<!--<li><a href="#"><span class="icon"><i class="fa fa-comment"></i></span></a></li>-->
                            	<li><span class="icon"><i class="fa fa-phone"></i></span><?php echo $user['phone_no'];?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 detail-txt">
                    <p>
                        <!--Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, aute irure dolor in reprehenderit in voluptate pariatur Ut enim ad minim veniam, aute irure dolor in reprehenderit in voluptate pariatur-->
                    </p>
                </div>
                    	
                <div class="col-md-3">
                	<div class="share-button">
                    	<a href="#" class="btn btn-default btn-lg"><span class="icon"><i class="fa fa-share-alt"></i></span>Share</a>
                    </div>
                </div>
                    
                
            </div>
        </div>
    </section>
	<section class="carousel-wrapper">
		<section class="customer-carousel-section container">
			<div class="row">
			<div class="col-md-12">
            <!--Tab block start here-->
            <ul id="myTab" class="nav nav-tabs activity">
               <li class="active">
                  <a href="#Activity" data-toggle="tab" id="activity_tab">Activity</a>
               </li>
               <li><a href="#Deals" data-toggle="tab" id="deal_tab">Deals</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
               <div class="tab-pane store-view-deal-tab fade in active" id="Activity">
               		<!-- deal starts here -->
                    <div class="row activity_listing">
                      <div id="actiity_listing_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
                    </div>
                    <!-- deal ends here -->
               </div>
               <div class="tab-pane store-view-tab fade" id="Deals">
                  <div class="product-list-wrapper">
                    <ul class="product-list row deal_listing" id="content">
                      <div id="deal_listing_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
                	 </ul>
                </div>
               </div>
           </div>
            
            <!--Tab block end here-->
			
			</div>
			</div>
		</section>
	</section>
	
	
	
	<script>
	$(":file").filestyle({buttonBefore: true});
	(function ($) {
  $('.spinner #btn1').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  });
  $('.spinner #btn2').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
  });
})(jQuery);
//for carousel
$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
	</script>
    </body>
