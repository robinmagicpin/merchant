<script type="text/javascript">
      var all_sku_deal = '<?php echo isset($user["all_store_deal"])?$user["all_store_deal"]:""; ?>';
    </script>
</head>
    <body>
    <script type="text/javascript" src="<?php echo base_url; ?>js/around_you.js?v=<?php echo JS_VERSION; ?>">
    </script>
   
        <section class="create-section-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="create-add-btn">
                        <a href="#" class="btn btn-primary">
                        Create Deal <span class="icon"><i class="fa fa-plus"></i></span>
                        </a>
                    </div>
                    </div>
                </div>
            </div>
        </section>	
	<section class="carousel-wrapper around-you-section">
		<section class="customer-carousel-section container">
			<div class="row">
			<div class="col-md-12">
            <!--Tab block start here-->
            <ul id="myTab" class="nav nav-tabs activity">
               <li class="active">
                  <a href="#Activity" data-toggle="tab" id="activity_tab">Activity</a>
               </li>
               <li><a href="#Deals" data-toggle="tab" id="deal_tab">Deals</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
               <div class="tab-pane store-view-deal-tab fade in active" id="Activity">
               		<!-- deal starts here -->
                    <div class="row customers_list">
                        <div id="activity_listing_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
                    </div>
                    <!-- deal ends here -->
               </div>
               <div class="tab-pane store-view-tab fade" id="Deals">
                  <div class="product-list-wrapper">
                    <ul class="product-list row deal_listing" id="content">
                      <div id="deal_listing_loader" class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
                	 </ul>
                </div>
               </div>
           </div>
            
            <!--Tab block end here-->
			
			</div>
			</div>
		</section>
	</section>
	
	
	
	<script>
	$(":file").filestyle({buttonBefore: true});
	(function ($) {
  $('.spinner #btn1').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  });
  $('.spinner #btn2').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
  });
})(jQuery);
//for carousel
$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
	</script>
    </body>
