</head>
<body>
<?php global $login_statuses;?>
<?php $current_merchant_details = isset($user["merchant"])?str_replace("'","",json_encode($user["merchant"])):"{}" ;
$all_locations = str_replace("'","",json_encode($locations));
$current_merchant_images = isset($user["store_images"])?str_replace("'","",json_encode($user["store_images"])):"{}";
?>
<script>
var login_statuses = JSON.parse('<?php echo json_encode($login_statuses); ?>');
var user_status = '<?php echo $user["status"]; ?>';
var locations = JSON.parse('<?php echo $all_locations; ?>');
var merchant_images = JSON.parse('<?php echo $current_merchant_images; ?>');
var merchant = JSON.parse('<?php echo $current_merchant_details; ?>');



</script>
<script type="text/javascript" src="<?php echo base_url; ?>js/edit_profile.js?v=<?php echo JS_VERSION; ?>">
</script>

	<div id="profile_alert_messages">
		<div class="alert alert-success profile_update_success" role="alert" style="display:none;text-align:center;"></div>
		<div class="alert alert-danger profile_update_failure" role="alert" style="text-align:center;display:none;"></div>
	</div>
	<section class="main-container" id ="register-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="register-wrapper">
					<div class="title-bar"><h2><span class="icon"><i class="fa fa-pencil-square-o"></i>
						<?php if($user['status'] == $login_statuses['edit_profile']):?>
							</span>Create profile</h2></div>
						<?php else: ?>
							</span>Edit profile</h2></div>
						<?php endif ?>

					<div class="profile-detail-section">
					<div class="profile-thumb">
						
					<form method="post" class="image_form">
						<input type="file" class="filestyle" name="store_image" accept="image/jpeg,.webp" data-buttonBefore="true">
						<!--<input class="btn btn-default image_upload_button" placeholder="" type="submit" id="image_submit">-->
						<!--<div class="edit_profile_image_upload_loader image_upload_button"><i class="fa fa-cog fa-spin"></i></div>-->
					</form>
					
					</div>
					<div class="profile-tag">
						Upload store pictures here
					</div>
					
					</div>
						<form class="form" method="post" >
							<div class="form-group">
								<label>USER NAME<sup>*</sup></label>
								<?php if($user['status'] == $login_statuses['edit_profile']):?>		
							  		<input class="form-control" placeholder="" type="text" name="user_name" required>

							  	<?php else: ?>
							  		<input class="form-control" placeholder="" type="text" name="user_name" readonly="readonly" value="<?php echo $user['user_name']; ?>">
							  	<?php endif ?>
							  	<input id="chosen_user_name" type="text" value="User Name Already Exists." readonly="readonly" style="display:none;" />
							</div>
							<?php if($user['status'] == $login_statuses['edit_profile']):?>		
								<div class="form-group">
									<label>PASSWORD<sup>*</sup></label>
						  			<input class="form-control" placeholder="" type="password" name="password" required>
								</div>
							<?php endif ?>
						<div class="form-group">
							<label>STORE NAME<sup>*</sup></label>
						  <input class="form-control" placeholder=""  type="text" name="merchantName" value="<?php echo isset($user['merchant']['merchantName'])?$user['merchant']['merchantName']:''; ?>" required />
						</div>
						<div class="form-group">
						<label>Contact Person Name</label>
						  <input class="form-control" placeholder="" type="text" name="contactPerson" value="<?php echo isset($user['merchant']['contactPerson'])?$user['merchant']['contactPerson']:''; ?>" />
						</div>
						<div class="form-group">
						<label>Store Phone #</label>
						  <input class="form-control" placeholder="" type="text" name="landlineNumber" value="<?php echo isset($user['merchant']['landlineNumber'])?$user['merchant']['landlineNumber']:''; ?>" />
						</div>
						<div class="form-group">
							<label>Mobile #</label>
							<input class="form-control" readonly="readonly" placeholder="" type="text" name="mobile" value="<?php echo $user['phone_no'];?>">
						</div>
						<div class="form-group">
						<label>Street Address<sup>*</sup></label>
						  <input class="form-control" placeholder="" type="text" name="addressLine1" value="<?php echo isset($user['merchant']['addressLine1'])?$user['merchant']['addressLine1']:''; ?>" required />
						</div>
						<div class="form-group child">
						  <select class="selectpicker" name="state" ><sup>*</sup>
						  	<option value=""> </option>
							<?php  foreach ($locations['all_states'] as $value): ?>
						  		<option value="<?php echo $value; ?>"><?php echo $value; ?></option>
						  	<?php endforeach ?>
						  </select>
						</div>
						<div class="form-group child">
						  <select name="city" ><sup>*</sup>
						  <option value=""> </option>
						  </select>
						</div>
						<div class="form-group child">
						  <select name="locality"><sup>*</sup>
						  <option value=""> </option>
						  </select>
						</div>
						<!--<div class="social-links">
							<ul>
								<li class="fb"><a href="#">Facebook</a></li>
								<li class="twitter"><a href="#">Twitter</a></li>
							</ul>

						 </div>-->
						<div class="form-group form_buttons" >
						  <div class="pull-right btn-group">
						  
						  <?php if($user['status'] == $login_statuses['edit_profile']):?>
						  	<button class="btn btn-default" id="save_profile">Save</button>
						  <?php else: ?>
						  	<button class="btn btn-default" id="save_profile">Update</button>
						  <?php endif; ?>
						  <?php if($user['status'] != $login_statuses['edit_profile']):?>
						  	<button  class="btn btn-default" onclick="window.history.back();">Cancel</button>
						  <?php endif ?>
						  </div>
						</div>
						<?php if($user['status'] == $login_statuses['dashboard']):?>
						<div class="form-group">
						  	<div class="pull-right reset-password"><a href="<?php echo base_url.'login/edit_password' ?>">Reset Password</a></div>
						</div>
						<?php endif ?>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<div>
<?php if(isset($user['store_name'])): ?>
	Welcome <?php echo $$user['store_name']; ?>
<?php endif ?>
</div>
</div>
<script>
	$(":file").filestyle({buttonBefore: true});
</script>

</script>
</body>
