</head>
<body>
<script>
var login_statuses = JSON.parse('<?php echo json_encode($login_statuses); ?>');
var user_status = '<?php echo $user["status"]; ?>';



</script>
<script type="text/javascript" src="<?php echo base_url; ?>js/edit_password.js?v=<?php echo JS_VERSION; ?>">
</script>

	<div id="profile_alert_messages">
		<div class="alert alert-success profile_update_success" role="alert" style="display:none;text-align:center;"></div>
		<div class="alert alert-danger profile_update_failure" role="alert" style="text-align:center;display:none;"></div>
	</div>
	<section class="main-container" id ="register-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="register-wrapper">
					<div class="title-bar"><h2><span class="icon"><i class="fa fa-pencil-square-o"></i>
						</span>Change Password</h2>
					</div>

					<div class="profile-detail-section">
					</div>
						<form class="form" method="post" id="reset_password_form">
								<div class="form-group">
									<label>NEW PASSWORD<sup>*</sup></label>
						  			<input class="form-control" type="password" name="new_password" required>
								</div>
						<div class="form-group">
							<label>Confirm PASSWORD<sup>*</sup></label>
						  <input class="form-control"  type="password" name="confirm_password" value="" required />
						</div>
						
						
						<div class="form-group form_buttons" >
						  <div class="pull-right btn-group">
						  	<button class="btn btn-default" id="save_password">Save</button>
						  	<button  class="btn btn-default" onclick="window.history.back();">Cancel</button>
						  </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>
<div>
<?php if(isset($user['store_name'])): ?>
	Welcome <?php echo $$user['store_name']; ?>
<?php endif ?>
</div>
</div>
<script>
	$(":file").filestyle({buttonBefore: true});
</script>

</script>
</body>
