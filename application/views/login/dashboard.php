    <script type="text/javascript">
    	var all_sku_deal = '<?php echo isset($user["all_store_deal"])?$user["all_store_deal"]:""; ?>';
    </script>
    </head>
    <body>
    <script type="text/javascript" src="<?php echo base_url; ?>js/merchant_dashboard.js?v=<?php echo JS_VERSION; ?>">
    </script>
    <section class="create-section-wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="create-add-btn">
				<a href="#" class="btn btn-primary">
				Create Deal <span class="icon"><i class="fa fa-plus"></i></span>
				</a>
			</div>
			</div>
		</div>
	</div>
	</section>
     
    
    <section class="performance-check">
	<!--<section class="performance-check">-->
		<div class="container inner-wrapper">
			<div class="row">
				<div class="col-md-12">
					<h2 class="lead">My Performance over : </h2>
					<section class="support-links dashboard-support-links">
            			<div class="row">
				            <ul class="breadcrumbs col-md-8">
                    			<li class="active today_time" id="month_time_button"><a href="#">30 Days</a></li>
                    			<li class="week_time" id="week_time_button"><a href="#">7 Days</a></li>
                    			<li class="all_time" id="all_time_button"><a href="#">Over all</a></li>
			                </ul>
			            </div>
    				</section>
				
					<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="transation-wrapper" data-toggle="tooltip" data-placement="bottom" title="Number of verified orders from magicpin">
								<div class="transaction-amount transaction-count">
									<?php echo $dashboard_summary['transactionCount']; ?>
								</div>
								<div class="transaction-details" >
								<span class="icon"><i class="fa fa-user"></i></span>Transactions
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="transation-wrapper" data-toggle="tooltip" data-placement="bottom" title="Total amount spent by verified magicpin customers">
								<div class="transaction-amount transaction-sales">
									₹<?php echo $dashboard_summary['totalSales']; ?>
								</div>
								<div class="transaction-details" >
								<span class="icon"><i class="fa fa-inr"></i></span>Sales
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="transation-wrapper" data-toggle="tooltip" data-placement="bottom" title="Total value of rewards given to magicpin customers">
								<div class="transaction-amount transaction-funds">
									₹<?php echo $dashboard_summary['funding']; ?>
								</div>
								<div class="transaction-details">
								<span class="icon"><i class="fa fa-inr"></i></span>Marketing Spent
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12">
							<div class="transation-wrapper" data-toggle="tooltip" data-placement="bottom" title="Balance left in your wallet">
								<div class="transaction-amount transaction-balance">
									₹<?php echo '0'; ?>
								</div>
								<div class="transaction-details">
								<span class="icon"><i class="fa fa-inr"></i></span>Wallet
								</div>
							</div>
						</div>
					</div>
					<div class="row">
					<div class="col-md-12">
						<div class="pull-right btn-group-top">
							<a href="#" class="btn btn-default">
							<span class="icon"><i class="fa fa-plus"></i></span>
							Add Balance
							</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="carousel-wrapper">
		<section class="customer-carousel-section container">
			<div class="row">
			<div class="col-md-12">
			<div class="title-bar"><h3><span class="icon"><i class="fa fa-user"></i></span>My Customers</h3><a href="<?php echo base_url; ?>customer/" class="right-link">See All</a></div>
			<!-- carousel starts here -->
			<div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel">
  <div class="carousel-inner customers_list ">
   	<div class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-circle-left"></i></a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-circle-right"></i></a>
</div>
			<!-- carousel ends here -->
			</div>
			</div>
		</section>
		
		<!-- around you section -->
		<section class="customer-carousel-section container">
			<div class="row">
			<div class="col-md-12">
			<div class="title-bar"><h3><span class="icon"><i class="fa fa-map-pin"></i>
</span>Around Me</h3><a href="<?php echo base_url; ?>store/around_you/" class="right-link">See All</a></div>
			<!-- carousel starts here -->
			<div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="myCarousel1">
  <div class="carousel-inner around_you">
  	<div class="customer-div-ajax-loader"><i class="fa fa-spinner fa-pulse"></i></div>
    
  </div>
  <a class="left carousel-control" href="#myCarousel1" data-slide="prev"><i class="fa fa-chevron-circle-left"></i></a>
  <a class="right carousel-control" href="#myCarousel1" data-slide="next"><i class="fa fa-chevron-circle-right"></i></a>
</div>
			<!-- carousel ends here -->
			</div>
			</div>
		</section>
	</section>
<script>
	$(":file").filestyle({buttonBefore: true});
	(function ($) {
	$('.spinner #btn1').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  	});
  	$('.spinner #btn2').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
  	});
  	$(function () { $("[data-toggle = 'tooltip']").tooltip(); });
})(jQuery);

	</script>
    </body>
</html>
