</head>
<body>
	<script type="text/javascript" src="<?php echo base_url; ?>js/login.js?v=<?php echo JS_VERSION; ?>"></script>
	<header class="header" id ="header">
		<div class="header-top">
			<div class="inner-wrapper container">
				<div class="row">
					<div class="col-md-12">
						<a href="<?php echo base_url; ?>login/dashboard/">
							<div class="logo">
								<img src="<?php echo base_url; ?>images/logo.png" alt="magicpin" />
							</div>
						</a>
						<a href="#"  class="become_member pull-right partner-link">
							Become a partner
						</a>
						<a href="#" style="display:none;" class="login_screen pull-right partner-link">
							Already a partner?
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="main-container login-container" id ="login-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="login-wrapper login_div">
					<div class="welcome-logo"><img src="<?php echo base_url; ?>images/wel-logo.png" alt="magicpin" />
					</div>
					<h2 class="login_error">Please Enter your login details</h2>
					<form method="post" class="login_form">
					<div class="form-group user_name">
					  <input class="form-control" placeholder="ENTER USER ID OR PHONE NO" type="text" required />
					</div>
						
					<div class="form-group user_password">
					   <input class="form-control" placeholder="PASSWORD" type="password" required />
					</div>
					<div class="forgot-password"><span class="pull-right"><a href="#" class="forget_password"><em>Forgot password</em></a></span></div>
					<div class="form-group">
						<button class="btn btn-primary btn-lg btn-block login_me">Log In</button>
					</div>
					</form>
				</div>
				<div class="login-wrapper register_div" style="display:none;">
					<div class="welcome-logo"><img src="<?php echo base_url; ?>images/wel-logo.png" alt="magicpin" /></div>
					<h2 class="register-wrapper-title">Please Enter your 10 digit Phone No.</h2>
					<form method="post" class="register_form">
						<div class="form-group enter_phone_no">
						   <input class="form-control" placeholder="ENTER 10 digit PHONE NO." pattern="[789][0-9]{9}" type="text" required >
						</div>
						<button class="btn btn-primary btn-lg btn-block enter_phone_otp1">Proceed</button>
					</form>
					<form method="post" class="register_otp_form" style="display:none;">
						<div class="form-group enter_otp">
					    	<input class="form-control" placeholder="OTP" type="password" required >
						</div>
					    <button class="btn btn-primary btn-lg btn-block enter_phone_otp2">Proceed</button>
					</form>
						</div>
				</div>
				<div class="login-wrapper forget_div" style="display:none;"> 
					<div class="welcome-logo"><img src="<?php echo base_url; ?>images/wel-logo.png" alt="magicpin" /></div>
					<h2 class="forget-wrapper-title">Please Enter your 10 digit Phone No.</h2>
						<form method="post" class="form center-block forget_password_form1" >
						<div class="form-group fp_phone_no" >
						  <input class="form-control" placeholder="ENTER PHONE NO." pattern="[789][0-9]{9}" type="text" required>
						</div>
						<button class="btn btn-primary btn-lg btn-block fp_done1">Proceed</button>
						</form>
						<form method="post" class="form center-block forget_password_form2" style="display:none;">
						<div class="form-group fp_otp" >
						  <input class="form-control" placeholder="OTP" type="password" required>
						</div>
						<div class="fp_enter_passwords">
							<div class="form-group fp_new_password">
						  		<input class="form-control" placeholder="NEW PASSWORD" type="password" required>
							</div>
							<div class="form-group fp_new_password_confirm">
						  		<input class="form-control" placeholder="REPEAT NEW PASSWORD" type="password" required>
							</div>
							<button class="btn btn-primary btn-lg btn-block fp_done2">Proceed</button>
						</div>
						</form>
						  
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</section>

<!--<div>
	<p id="login_error"></p>
	<div id="first_login_div">
		<div class="user_name">User Name</td><td><input type="text"  /></div>
		<div class="user_password">Password</td><td><input type="password" /></div>
		<input type="submit" class="login_me" value="Done" />
	</div>
	<div>
		<button id="register">Become A Member</button>
		<button id="login" style="display:none;">Already have an account</button>
		<button id="forget_password" >Forget Password</button>
	</div>
	<div style="display:none" id="first_register_div">
		<div class="enter_phone_no">Enter Phone no: <input type="text" /></div>
		<div style="display:none;" class="enter_otp">Enter the Otp: <input type="text" /></div>
		<button class="enter_phone_otp">Done</button>
	</div>
	<div style="display:none;" id="fp_div">
		<div class="fp_phone_no">Enter Phone no: <input type="text" /></div>
		<div class="fp_otp" style="display:none;">Enter Otp: <input  type="text" /></div>
		<div class="fp_enter_passwords" style="display:none;">
			<div class="fp_new_password" >Enter New Password: <input type="password" /></div>
			<div class="fp_new_password_confirm">Enter New Password: <input type="password" /></div>
		</div>
		<button class="fp_done">Done</button>
	</div>-->
<script>
	var offsetHeight = document.getElementById('header').offsetHeight;
	var x = screen.availHeight - offsetHeight;
	document.getElementById('login-container').style.height = x + "px";
</script

</div>
</body>
